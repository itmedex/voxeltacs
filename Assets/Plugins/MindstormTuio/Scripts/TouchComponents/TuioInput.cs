/*
Unity3d-TUIO connects touch tracking from a TUIO to objects in Unity3d.

Copyright 2011 - Mindstorm Limited (reg. 05071596)

Author - Simon Lerpiniere

This file is part of Unity3d-TUIO.

Unity3d-TUIO is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Unity3d-TUIO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.

You should have received a copy of the GNU Lesser Public License
along with Unity3d-TUIO.  If not, see <http://www.gnu.org/licenses/>.

If you have any questions regarding this library, or would like to purchase 
a commercial licence, please contact Mindstorm via www.mindstorm.com.
*/

using System.Collections;
using UnityEngine;
using System.Linq;
using System;

/// <summary>
/// Provides TUIO input as UnityEngine.Touch objects for receiving touch information
/// Must be attached to a GameObject in the Hierachy to be used.
/// 
/// Provides exactly the same interface as UnityEngine.Input regarding touch data
/// allowing any code using UnityEngine.Input to use TuioInput instead.
/// 
/// Artur: Changed to include all regular Unity touches and Mouse clicks, alongside the TUIO ones.
/// </summary>
public class TuioInput : MonoBehaviour
{
	static TuioComponentBase tuioTracking;
    static TuioComponentBase mouseSim;

	static Touch[] allTouches = new Touch[0];
	
	public static readonly bool multiTouchEnabled = true;

    public static bool tuioEnabled
    {
        get;
        private set;
    }
	
	public static int touchCount
	{
		get;
		private set;
	}
	
	void Awake()
	{
		tuioTracking = InitTracking(new TuioTrackingComponent());
        mouseSim = InitTracking(new MouseTrackingComponent());

        tuioEnabled = true;
	}
	
	void Update()
	{
		if (tuioTracking == null) 
		{
            Debug.Log("TUIO not found");
			enabled = false;
            tuioEnabled = false;
			return;
		}

		TuioComponentBase tr = tuioTracking;
		UpdateTouches(tr);

        // Artur: also include fake mouse-simulated touches, if there aren't any legit touches
        if (touchCount == 0)
            AddMouseTouches(mouseSim);
	}
	
	void UpdateTouches(TuioComponentBase tr)
	{
		tr.BuildTouchDictionary();
		Touch[] tuioTouches = tr.AllTouches.Values.Select(t => t.ToUnityTouch()).ToArray();

        // Artur: include all touches on the same array
        Touch[] normalTouches = Input.touches;

        allTouches = tuioTouches.Concat(normalTouches).ToArray();
        touchCount = allTouches.Length;
	}

    void AddMouseTouches(TuioComponentBase tr)
	{
		tr.BuildTouchDictionary();
		Touch[] mouseTouches = tr.AllTouches.Values.Select(t => t.ToUnityTouch()).ToArray();

        allTouches = allTouches.Concat(mouseTouches).ToArray();
        touchCount = allTouches.Length;
	}
	
	TuioComponentBase InitTracking(TuioComponentBase tr)
	{
		tr.ScreenWidth = Camera.main.pixelWidth;
		tr.ScreenHeight = Camera.main.pixelHeight;
		return tr;
	}
	
	public static Touch GetTouch(int index)
	{
		return allTouches[index];		
	}
	
	public static Touch[] touches
	{
		get
		{
			return allTouches;
		}
	}
	
	void OnApplicationQuit()
	{
		if (tuioTracking != null) tuioTracking.Close();
        if (mouseSim != null) mouseSim.Close();

	}
}