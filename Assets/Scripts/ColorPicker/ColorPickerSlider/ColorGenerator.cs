﻿using UnityEngine;
using System.Collections;

public class ColorGenerator : MonoBehaviour {
	
	private GameObject colorPicker;
	private GameObject[] cursores;
	public GameObject background;
	public Background back;
	public RayMarching volume;

	private int nr_cursor;
	private int last_number;
	public int[] defined;
	public Color[] colors;

	//Vector para a aplicaçao da cor
	public float[] applycolor;

	//
	Vector3 back_pos;
	float left;
	float right;

	float check_time;
	int touches;

	bool first = true;

	//remote
	public bool remote_send;

	void Start () {
		colorPicker = GameObject.FindGameObjectWithTag("color_picker");

		nr_cursor = 0;
		last_number = 0;
		defined = new int[256];
		colors = new Color[256];

		back_pos = background.transform.position;
		left = back_pos.x - (background.transform.localScale.x / 2);
		right = back_pos.x + (background.transform.localScale.x / 2);
	}
	

	void FixedUpdate () {

		if(remote_send) {
			if (first) {
				left = back_pos.x - (background.transform.localScale.x / 2);
				right = back_pos.x + (background.transform.localScale.x / 2);
				first = false;
			}

			cursores = GameObject.FindGameObjectsWithTag("color_cursor");
			nr_cursor = cursores.Length;

			bool use = false;

			foreach (GameObject cursor in cursores) {
				if(cursor.GetComponent<ColorCursor>().change)
					use = true;
			}

			if (nr_cursor != last_number) {
				last_number = nr_cursor;
				Update_color(true);
			}

			if (nr_cursor == 0 || !use) {
				colorPicker.transform.position = new Vector3 ( -100f, -311f, -400f);
			}
		}
	}

	void Update_color (bool remote){

		defined = new int[256];
		colors = new Color[256];

		foreach (GameObject cursor in cursores) {

			float pos = cursor.transform.position.x;

			int posint = Mathf.RoundToInt(((pos-left)/(right-left))*255);

			defined[posint]= 1;
			colors[posint] = cursor.GetComponent<ColorCursor>().selected_color;
		}

		if (defined [0] == 0) {
			defined[0]=1;
			colors[0]= Color.black;
		}

		if (defined [255] == 0) {
			defined[255]=1;
			colors[255]= Color.white;
		}

		bool begin = false;

		for (int i=0; i < defined.Length; i++) {

			if (defined[i]==1 ){
				begin = true;
			}

			if (defined[i] == 0 && begin){

				// Encontrar proximo ponto com cor
				int j = i + 1;

				while(j < defined.Length && defined[j]==0){
					j++;
				}

				if(j<256){
					//print ("lerped");
					// Interpolar linearmente as cores entre os pontos
					float fj = j;
					float fi = i;
					float t = 1/(fj-fi+1f);
					defined[i] = 1;
					colors[i] = Color.Lerp(colors[i-1], colors[j],t);
				}
			}

		}

		if (remote) {
			float[] color_send = new float[cursores.Length*5 + 1];
			color_send[0] = cursores.Length;

			int idx = 1;

			foreach (GameObject cursor in cursores) {
				
				float pos = cursor.transform.position.x;
				
				int posint = Mathf.RoundToInt(((pos-left)/(right-left))*255);

				color_send[idx] = posint;

				Color s_color = cursor.GetComponent<ColorCursor>().selected_color;
				color_send[idx+1] = s_color.r;
				color_send[idx+2] = s_color.g;
				color_send[idx+3] = s_color.b;
				color_send[idx+4] = s_color.a;

				idx += 5;
			}

			GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
			nex.SendMessage ("Send_function_color_array", color_send, SendMessageOptions.DontRequireReceiver);
			back.SendMessage ("Apply_color", SendMessageOptions.DontRequireReceiver);
		} else {
			//volume.SendMessage ("Apply_color", colors,SendMessageOptions.DontRequireReceiver);
			back.SendMessage ("Apply_color", SendMessageOptions.DontRequireReceiver);
		}
	}

	void Receive_color_update(float[] color_rec){

		defined = new int[256];
		colors = new Color[256];

		for (int i=1; i<color_rec[0]*5 + 1; i+=5) {
			int idx = Mathf.RoundToInt(color_rec[i]);

			defined[idx]=1;
			Color cor = Color.white;
			cor.r = color_rec[i+1];
			cor.g = color_rec[i+2];
			cor.b = color_rec[i+3];
			cor.a = color_rec[i+4];
			colors[idx]=cor;
		}

		if (defined [0] == 0) {
			defined[0]=1;
			colors[0]= Color.black;
		}
		
		if (defined [255] == 0) {
			defined[255]=1;
			colors[255]= Color.white;
		}
		
		bool begin = false;
		for (int i=0; i < defined.Length; i++) {
			
			if (defined[i]==1 ){
				begin = true;
			}
			
			if (defined[i] == 0 && begin){
				// Encontrar proximo ponto com cor
				int j = i + 1;
				
				while(j < defined.Length && defined[j]==0){
					j++;
				}
				
				if(j<256){
					//print ("lerped");
					// Interpolar linearmente as cores entre os pontos
					float fj = j;
					float fi = i;
					float t = 1/(fj-fi+1f);
					defined[i] = 1;
					colors[i] = Color.Lerp(colors[i-1], colors[j],t);
				}
			}
			
		}

		volume.SendMessage ("Apply_color", colors,SendMessageOptions.DontRequireReceiver);
		//back.SendMessage ("Apply_color", SendMessageOptions.DontRequireReceiver);
	}

	void Deselect_all(){
		GameObject[] cursores = GameObject.FindGameObjectsWithTag("color_cursor");
		foreach (GameObject cursor in cursores) {
			cursor.SendMessage ("Deselect", SendMessageOptions.DontRequireReceiver);
		}
	}
}