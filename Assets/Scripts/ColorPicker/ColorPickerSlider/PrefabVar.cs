﻿using UnityEngine;
using System.Collections;

public class PrefabVar : MonoBehaviour {

	public GameObject left;
	public GameObject right;
	public GameObject window;

	// Variaveis de interesse
	public float l_position;
	public float r_position;
	public Color selected_color;
	

	void Update () {

		l_position = left.transform.position.x;
		r_position = right.transform.position.x;

		selected_color = window.GetComponent<Renderer>().material.color;
	}
}
