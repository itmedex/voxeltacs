﻿using UnityEngine;
using System.Collections;

public class ThumbColor : MonoBehaviour {
	
	public GameObject SB_thumb;

	// Update is called once per frame
	void Update () {
		Color rbg_color = SB_thumb.GetComponent<Renderer>().material.color;
		HSBColor hsb_color = HSBColor.FromColor (rbg_color);
		hsb_color.s = 1f;
		hsb_color.b = 1f;
		Color new_color = HSBColor.ToColor (hsb_color);

		GetComponent<Renderer>().material.color = new_color;
	}
}
