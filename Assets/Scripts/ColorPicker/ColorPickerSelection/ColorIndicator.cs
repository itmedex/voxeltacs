using UnityEngine;

public class ColorIndicator : MonoBehaviour {

	HSBColor color;
	public ColorGenerator intens_color;
	public Custom_Function_Hist2D grad_color;

	void Start() {
		color = HSBColor.FromColor(GetComponent<Renderer>().sharedMaterial.GetColor("_Color"));
		transform.parent.BroadcastMessage("SetColor", color);
	}

	void ApplyColor ()
	{
		GetComponent<Renderer>().sharedMaterial.SetColor ("_Color", color.ToColor());
		if (intens_color != null)
			intens_color.gameObject.SendMessage ("Update_color", false, SendMessageOptions.DontRequireReceiver);
		if (grad_color != null)
			grad_color.gameObject.SendMessage ("Update_color", false, SendMessageOptions.DontRequireReceiver);
		transform.parent.BroadcastMessage("OnColorChange", color, SendMessageOptions.DontRequireReceiver);
	}

	void SetHue(float hue)
	{
		color.h = hue;
		ApplyColor();
    }	

	void SetSaturationBrightness(Vector2 sb) {
		color.s = sb.x;
		color.b = sb.y;
		ApplyColor();
	}
}