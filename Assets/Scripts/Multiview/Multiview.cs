﻿using UnityEngine;
using System.Collections;

public class Multiview : MonoBehaviour {

	//Vector de camaras
	public GameObject[] cameras;

	// Modelo a instanciar em frente a cada objecto
	private GameObject model;

	// Origem dos vectores do sistema de particulas
   //	public Grapher5Up main;

	// Sistema de particulas
	private ParticleSystem.Particle[] original;
	private ParticleSystem.Particle[] apply;
	private Color[,,] cores;
	private Vector3[] pos_original;
	private float[] histograma;

	private Vector3 size;

	private float scalex = 1f;
	private float scaley = 1f;
	private float scalez = 1f;

	// Use this for initialization
	void Start () {
		histograma = new float[256];
	}

	void Create_views(){

//		original = main.thumb_points;

		// Instanciar e posicionar o modelo
		for (int nr = 0; nr < cameras.Length; nr++) {

			Vector3 worldpos = cameras[nr].transform.position;
			worldpos.x = worldpos.x;
			worldpos.y = worldpos.y;
			worldpos.z = worldpos.z + 1f; 

			model = (GameObject)Instantiate (Resources.Load ("Multigraph"));

			model.transform.position = worldpos;
			model.transform.eulerAngles = new Vector3( 270f, 0, 0);

			apply = original;

			// definir o intervalo
			float intervalo = 255f / cameras.Length;
			int pontoi = Mathf.RoundToInt(nr*intervalo);
			int pontof = Mathf.RoundToInt((nr+1)*intervalo);

			// definir as particulas
			for (int i = 0; i < original.Length; i++) {
				// Vai buscar intensidade original
				/*Vector3 po = pos_original[i];
				int x = Mathf.RoundToInt(po.x);
				int y = Mathf.RoundToInt(po.y);
				int z = Mathf.RoundToInt(po.z);
				Color c = cores[x, y, z];*/
				// Cor do modelo
				Color c_m = apply [i].color;
				//int gray_value = Mathf.FloorToInt(255*(c_m.r + c_m.g + c_m.b)/3);
				int gray_value = Mathf.FloorToInt(255f * c_m.grayscale);

				if(gray_value < pontoi || gray_value > pontof){
					c_m.a = 0f;
				}
				if(gray_value >= pontoi && gray_value <= pontof){
					c_m.a = 1f;
				}
				if(gray_value == 0) c_m.a = 0f;

				apply [i].color = c_m;
			}


			model.GetComponent<ParticleSystem>().SetParticles (apply, original.Length);
		}
	}

	void Set_size(Vector3 dim){
		size = dim;
	}

	void Set_scaley(Vector3 scale){
		scalex = scale.x;
		scaley = scale.z;
		scalez = scale.y;
	}

	void Create_thumbnail(Color[,,] colors){

		// Codigo para criar um tensor de cores
		float nr_points = 25f;

		float increment = 1f / (nr_points - 1f);

		float incrementy = scaley / (nr_points - 1f);
		float incrementz = scalez / (nr_points - 1f);

		float radius = 2*increment;
		
		int nt_particles = Mathf.RoundToInt (nr_points*nr_points*nr_points);
		//print (nt_particles);

		ParticleSystem.Particle[] points = new ParticleSystem.Particle[nt_particles];

		int i = 0;
		for (float z = 0; z < nr_points; z++) {
				for (float x = 0; x < nr_points; x++) {
					for (float y = 0; y < nr_points; y++) {
						
					// Posiçao dos pontos
						Vector3 p = new Vector3();
						
						p.x = (x * increment) - 1f/2f;
						p.y = (y * incrementy) - scaley/2f;
						p.z = (z * incrementz) - scalez/2f;
						
						points[i].position = p;

					// Cor dos pontos

					int px = Mathf.RoundToInt( x * size.x /(nr_points-1));
					int py = Mathf.RoundToInt( y * size.y /(nr_points-1));
					int pz = Mathf.RoundToInt( z * size.z /(nr_points-1));

					points[i].color = colors[px,py,pz];
					//points[i].color= Color.red;
						
					// Raio dos pontos
					points[i].size = radius;
						
					// Histograma
					Color c = colors[px,py,pz];
					int intens = Mathf.RoundToInt(255f*(c.r+c.g+c.b)/3f);
					histograma[intens] += 1f;
					//
						i++;
					}
				}
		}

		// Instanciar e posicionar os modelos - versao 1
		/*
		for (int nr = 0; nr < cameras.Length; nr++) {
			
			Vector3 worldpos = cameras[nr].transform.position;
			worldpos.x = worldpos.x;
			worldpos.y = worldpos.y;
			worldpos.z = worldpos.z +1.5f; 
			
			model = (GameObject)Instantiate (Resources.Load ("Multigraph"));
			
			model.transform.position = worldpos;

			// Definir o intervalo
			float intervalo = 255f / cameras.Length;
			int pontoi = Mathf.RoundToInt(nr*intervalo);
			int pontof = Mathf.RoundToInt((nr+1)*intervalo);
			
			//print (points.Length);
			// definir as particulas
			for (int j = 0; j < points.Length; j++) {

				Color c_m = points [j].color;

				int gray_value = Mathf.RoundToInt(255f * (c_m.r + c_m.g + c_m.b) / 3f );
				
				if(gray_value < pontoi || gray_value > pontof){
					c_m.a = 0f;
				}
				if(gray_value >= pontoi && gray_value <= pontof){
					c_m.a = 1f;
				}
				if(gray_value == 0) c_m.a = 0f;

				points [j].color = c_m;
			}
			
			
			model.GetComponentInChildren<ParticleSystem>().SetParticles (points, points.Length);
		}
		*/
		// Instanciar e posicionar os modelos - versao 2
		for (int nr = 0; nr < cameras.Length; nr++) {
			
			Vector3 worldpos = cameras[nr].transform.position;
			worldpos.x = worldpos.x;
			worldpos.y = worldpos.y;
			worldpos.z = worldpos.z +2.0f; 
			
			model = (GameObject)Instantiate (Resources.Load ("Multigraph"));
			
			model.transform.position = worldpos;
			
			// Definir o intervalo
			float intervalo = 255f / cameras.Length;
			int pontoi = Mathf.RoundToInt(nr*intervalo);
			int pontof = Mathf.RoundToInt((nr+1)*intervalo);

			// Contar o numero de pontos que sao usados
			int nr_part = 0;

			for (int j = 0; j < points.Length; j++) {
				Color c_m = points [j].color;
				int gray_value = Mathf.RoundToInt(255f * (c_m.r + c_m.g + c_m.b) / 3f );
				if(gray_value >= pontoi && gray_value <= pontof){
					nr_part++;
				}
			}

			// Definir as particulas num vector novo
			ParticleSystem.Particle[] pontos = new ParticleSystem.Particle[nr_part];

			int idx = 0;
			for (int j = 0; j < points.Length; j++) {
				
				Color c_m = points [j].color;
				
				int gray_value = Mathf.RoundToInt(255f * (c_m.r + c_m.g + c_m.b) / 3f );

				if(gray_value >= pontoi && gray_value <= pontof && gray_value != 0){
					pontos[idx].color = points[j].color;
					pontos[idx].position = points[j].position;
					pontos[idx].size = points[j].size;
					idx++;
				}
			}

			model.GetComponentInChildren<ParticleSystem>().SetParticles (pontos, pontos.Length);
		}
	}
}
