﻿using UnityEngine;
using System.Collections;
//using System.Math;

public class SpatialSlicing : MonoBehaviour {

	// Target
	public GameObject targetTablet;
	public GameObject camera;
	private RayMarching rayMarching;
	
	private Vector3 initPosTablet;
	private Vector3 initClip;
	private float[] teste = new float[2];
	
	void Start () {
		//teste [1] = 3.0f; //(bebaixo para cima)
		rayMarching = camera.GetComponent<RayMarching>();
		initClip = new Vector3 (rayMarching.getClipDimensions().x, rayMarching.getClipDimensions().y, rayMarching.getClipDimensions().z);
		initPosTablet = new Vector3 (targetTablet.transform.position.x, targetTablet.transform.position.y, targetTablet.transform.position.z);
	}

	/* CORTES SEGUNDO TODOS OS EIXOS
	// Update is called once per frame
	void Update () {
		teste [1] = rayMarching.getWidgetFace ();
		switch(rayMarching.getWidgetFace()) {
			case 1: //costas
			case 6: //frente
				if(targetTablet.transform.position.z >= initPosTablet.z)
					teste [0] = System.Math.Abs(targetTablet.transform.position.z - initPosTablet.z)*100;
				else if(targetTablet.transform.position.z < initPosTablet.z)
					teste [0] = (targetTablet.transform.position.z - initPosTablet.z)*100;
				
				initPosTablet.z = targetTablet.transform.position.z;
				rayMarching.setSlicing (teste);
			break;
			case 2: // up
			case 3: // down
				if(targetTablet.transform.position.y >= initPosTablet.y)
					teste [0] = System.Math.Abs(targetTablet.transform.position.y - initPosTablet.y)*100;
				else if(targetTablet.transform.position.y < initPosTablet.y)
					teste [0] = (targetTablet.transform.position.y - initPosTablet.y)*100;
				
				initPosTablet.y = targetTablet.transform.position.y;
				rayMarching.setSlicing (teste);
			break;
			case 4: // left
			case 5: // right
				if(targetTablet.transform.position.x >= initPosTablet.x) {
					teste [0] = System.Math.Abs(targetTablet.transform.position.x - initPosTablet.x)*100;
				}	else if(targetTablet.transform.position.x < initPosTablet.x) {
					teste [0] = (targetTablet.transform.position.x - initPosTablet.x)*100;
				}

				initPosTablet.x = targetTablet.transform.position.x;
				rayMarching.setSlicing (teste);
			break;
			default:
			break;
		}
		//Position
		//gameObject.transform.position = new Vector3( initPosVolume.x + (targetTablet.transform.position.x - initPosTablet.x), initPosVolume.y + (targetTablet.transform.position.y - initPosTablet.y), initPosVolume.z + (targetTablet.transform.position.z - initPosTablet.z));
	}*/
	// Update is called once per frame
	void Update () {
		teste [1] = rayMarching.getWidgetFace ();
		switch(rayMarching.getWidgetFace()) {
		case 1: //costas
		case 2: // up
		case 3: // down
		case 4: // left
		case 5: // right
		case 6: //frente
			if(targetTablet.transform.position.z >= initPosTablet.z)
				teste [0] = System.Math.Abs(targetTablet.transform.position.z - initPosTablet.z)*100;
			else if(targetTablet.transform.position.z < initPosTablet.z)
				teste [0] = (targetTablet.transform.position.z - initPosTablet.z)*100;
			
			initPosTablet.z = targetTablet.transform.position.z;
			rayMarching.setSlicing (teste);
			break;
		default:
			break;
		}
		//Position
		//gameObject.transform.position = new Vector3( initPosVolume.x + (targetTablet.transform.position.x - initPosTablet.x), initPosVolume.y + (targetTablet.transform.position.y - initPosTablet.y), initPosVolume.z + (targetTablet.transform.position.z - initPosTablet.z));
	}

}
