﻿using UnityEngine;
using System.Collections;

public class SpatialMovement : MonoBehaviour {

	// Target
	public GameObject targetTablet;

	private Vector3 initPosTablet;
	private Vector3 initPosVolume;
	private Vector3 initialRotVolume;
	private Vector3 initialRotTablet;

	//Sensibilidades
	//public float rot_sentivity = 1;
	//public float zoom_sensitivity = 1;
	//public float trans_sensitivity = 1;
	
	void Awake () {
		initPosVolume = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y - 1 /*meter*/, gameObject.transform.position.z);
		initPosTablet = new Vector3 (targetTablet.transform.position.x, targetTablet.transform.position.y, targetTablet.transform.position.z);
		initialRotVolume = new Vector3 (gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y, gameObject.transform.eulerAngles.z);
		initialRotTablet = new Vector3 (targetTablet.transform.eulerAngles.x, targetTablet.transform.eulerAngles.y, targetTablet.transform.eulerAngles.z);
	}
	
	// Update is called once per frame
	void Update () {
		//Position
		gameObject.transform.position = new Vector3( initPosVolume.x + (targetTablet.transform.position.x - initPosTablet.x), initPosVolume.y + (targetTablet.transform.position.y - initPosTablet.y), initPosVolume.z + (targetTablet.transform.position.z - initPosTablet.z));
		// Rotation
		gameObject.transform.eulerAngles = new Vector3( initialRotVolume.x + (targetTablet.transform.eulerAngles.x - initialRotTablet.x), initialRotVolume.y + (targetTablet.transform.eulerAngles.y - initialRotVolume.y), initialRotVolume.z + (targetTablet.transform.eulerAngles.z - initialRotVolume.z));
	}
}