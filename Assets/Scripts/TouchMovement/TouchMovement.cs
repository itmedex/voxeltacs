﻿using UnityEngine;
using System.Collections;

public class TouchMovement : MonoBehaviour {

	public float rotate_threshold = 0;
	public float distinction_threshold = 4;
	public float second_distinction = 2;
	
	// Posiçao inicial dos dedos
	private Vector2 startPos;
	private Vector2 startPos_2;
	private Vector2 startPos_3;
	
	// Direcçao do primeiro dedo
	private Vector2 direction;
	private Vector2 direction_3;

	// Id do primeiro dedo
	private bool id_set;
	private int main_finger_id;
	
	// Distancia entre os dedos
	private float mutual_distance;
	private float distance;
	
	// Target
	public GameObject target;
	private Vector3 initialpos;
	private Vector3 initialrot;
	private Vector3 currentrot;
	private Vector3 initialrot_thumb;
	private Vector3 currentrot_thumb;

	//Sensibilidades
	
	public float rot_sentivity = 1;
	public float zoom_sensitivity = 1;
	public float trans_sensitivity = 1;
	
	// Bloqueio de ecra
	public bool unlocked = true;
	
	// Screen var
	private Vector3 initialcoor;
	private Vector3 finalcoor;
	private float plane_height;
	private float plane_lenght;
	private float screen_height;
	private float screen_lenght;
	
	// Variaveis para taps
	float check_time;
	int touches;
	public GameObject canvas;
	
	// Rotaçao do multiview
	private bool first = true;
	GameObject[] multy;
	
	// Box widjet
	public CameraWidget widget;
	
	//touch camera
	private bool move= true;

	//remote
	public bool remote_send;

	// Tablet Connection Object
	GameObject tab;
	
	void Start () {
	
		initialpos = target.transform.position;
		initialrot = new Vector3 (target.transform.eulerAngles.x, target.transform.eulerAngles.y, target.transform.eulerAngles.z);
		currentrot = new Vector3 (target.transform.eulerAngles.x, target.transform.eulerAngles.y, target.transform.eulerAngles.z);
		initialrot_thumb = new Vector3 (0, 0, 0);
		currentrot_thumb = new Vector3 (0, 0, 0);
		tab = GameObject.FindGameObjectWithTag ("Nexus");
		screenBounds ();
		
		first = true;

		if(widget != null)
			widget.OnCameraChanged += widget_OnCameraChanged;
	}

	void sendTransformData() {
		tab.SendMessage ("Send_function_tranform", target.transform, SendMessageOptions.DontRequireReceiver);
	}

	// Determinar dimensoes do ecra
	void screenBounds () {
		
		Camera cam = GetComponent<Camera>();
		
		screen_height = cam.pixelHeight;
		screen_lenght = cam.pixelWidth;
	}

	void widget_OnCameraChanged(Camera camera, string view, Vector3 directionn)
	{
		//Debug.Log("Registered " + view + "(" + directionn + ") on " + camera.name);
		//rigidbody.AddForce(direction * 100f);
		/*
		target.transform.LookAt (target.transform.position + directionn);

		foreach(GameObject uni in multy){
			uni.transform.LookAt (uni.transform.position + directionn);
		}
		*/
		
		if (directionn == Vector3.forward) {
			currentrot = initialrot + new Vector3 (0, 180f, 0);
			target.transform.localEulerAngles = currentrot;
			currentrot_thumb = initialrot_thumb + new Vector3 (0, 180f, 0);

			if (remote_send)
				sendTransformData();

			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = currentrot_thumb;
			}
		}
		
		if (directionn == Vector3.up) {
			currentrot = initialrot + new Vector3 (-90f, 0, 0);
			target.transform.localEulerAngles = currentrot;
			currentrot_thumb = initialrot_thumb + new Vector3 (-90f, 0, 0);

			if (remote_send)
				sendTransformData();

			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = currentrot_thumb;
			}
		}
		
		if (directionn == Vector3.down) {
			currentrot = initialrot + new Vector3 (90f, 0, 0);
			target.transform.localEulerAngles = currentrot;
			currentrot_thumb = initialrot_thumb + new Vector3 (90f, 0, 0);

			if (remote_send)
				sendTransformData();

			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = currentrot_thumb;
			}
		}
		
		if (directionn == Vector3.left) {
			currentrot = initialrot + new Vector3 (0, -90f, 0);
			target.transform.localEulerAngles = currentrot;
			currentrot_thumb = initialrot_thumb + new Vector3 (0, -90f, 0);

			if (remote_send)
				sendTransformData();

			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = currentrot_thumb;
			}
		}
		
		if (directionn == Vector3.right) {
			currentrot = initialrot + new Vector3 (0, 90f, 0);
			target.transform.localEulerAngles = currentrot;
			currentrot_thumb = initialrot_thumb + new Vector3 (0, 90f, 0);

			if (remote_send)
				sendTransformData();

			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = currentrot_thumb;
			}
		}
		
		if (directionn == Vector3.back) {
			currentrot = initialrot;
			target.transform.localEulerAngles = currentrot;
			currentrot_thumb = initialrot_thumb;

			if (remote_send)
				sendTransformData();

			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = currentrot_thumb;
			}
		}
		
	}

	// Update is called once per frame
	void Update () {
	
		// Vai identificar os thumbnails para modificar as suas posiçoes
		if (first) {
			multy = GameObject.FindGameObjectsWithTag("multigraph");
			first = false;
		}

		// Este codigo instancia o pequeno indicador na posiçao do toque
		/*
		if (TuioInput.touchCount > 0) {
			foreach( Touch touch in TuioInput.touches){
				if (touch.phase == TouchPhase.Began){
					GameObject dot = (GameObject)Instantiate (Resources.Load ("touched"));
					dot.transform.SetParent(canvas.transform, false);
					Vector3 dotpos = new Vector3 (touch.position.x, touch.position.y, 10f);
					dot.transform.position = dotpos;
					DestroyObject(dot,0.4f);
				}
			}
		}
		*/

		if (unlocked) {

			if (TuioInput.touchCount == 0) {
				id_set = false;
				main_finger_id = 0;
			}

			if (TuioInput.touchCount > 0) {

				if(!id_set){
					main_finger_id = TuioInput.touches[0].fingerId;
					id_set = true;
				}

				// Mudar para usar o finger id
				//Touch touch = TuioInput.touches [0];
				Touch touch = new Touch();
				bool set = false;
				foreach( Touch touch_1 in TuioInput.touches){
					if(touch_1.fingerId == main_finger_id) {
						touch = touch_1;
						set=true;
					}
				}
				// se nao encontrar nenhum touch com esse id, entao e porque levantou esse dedo
				// nesse caso, o primeiro dedo no vector vai ser usado
				if(!set){
					main_finger_id = TuioInput.touches[0].fingerId;
					touch = TuioInput.touches[0];
				}

				// Limita a interacçao a uma dada zona do ecra
				//move = true;
				//if(touch.position.y > 0.09f * screen_height){
				//	move = true;
				//}else{
				//	move = false;
				//}

				// Faz os controlos de rotaçao em x e z
				if (TuioInput.touchCount == 1 && move) {

					if (touch.phase == TouchPhase.Began) {

						startPos = touch.position;

						if (Time.time > check_time + 0.5f){
							touches = 1;
							check_time = Time.time;
						}else{
							touches = touches + 1;
							check_time = Time.time;
						}
						
						if (touches == 3){
							target.transform.position = initialpos;
							target.transform.eulerAngles = initialrot;
							currentrot = initialrot;

							if (remote_send)
								sendTransformData();

							foreach(GameObject uni in multy){
								uni.transform.eulerAngles = initialrot;
							}
							gameObject.GetComponent<Camera>().fieldOfView = 60;
							if (remote_send) {
								GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
								nex.SendMessage ("Send_function_fov", 60 , SendMessageOptions.DontRequireReceiver);
							}
						}
					}

					if (touch.phase == TouchPhase.Moved){

						direction = touch.position - startPos;

						// Rotaçao em x
						target.transform.Rotate ((direction.y/screen_height)* 360f * rot_sentivity, 0, 0, Space.World);

						// Rotaçao em z
						target.transform.Rotate (0, -(direction.x/screen_lenght)* 360f * rot_sentivity, 0, Space.World);
								
						if (remote_send)
							sendTransformData();

						foreach(GameObject uni in multy){
							uni.transform.rotation = target.transform.rotation;
						}

						/*
						float rotate_x = (direction.y/screen_height)* 360f * rot_sentivity;
						float rotate_y = (direction.x/screen_lenght)* 360f * rot_sentivity;

						currentrot.x = currentrot.x + rotate_x;
						if(currentrot.x >90f) currentrot.x = 90f;
						if(currentrot.x <-90f) currentrot.x = -90f;

						currentrot.y = currentrot.y - rotate_y;
						if(currentrot.y >180f) currentrot.y = 180f;
						if(currentrot.y <-180f) currentrot.y = -180f;

						target.transform.eulerAngles = currentrot;

						foreach(GameObject uni in multy){
							uni.transform.rotation = target.transform.rotation;
						}
						*/
					}
				}

				// Faz os controlos de rotaçao em y e zoom
				if (TuioInput.touchCount == 2 && move) {
					// Definir o segundo ponto
					Touch touch_2;
					if (TuioInput.touches[0].fingerId == main_finger_id){
						touch_2 = TuioInput.touches[1];
					}else{
						touch_2 = TuioInput.touches[0];
					}

					if(touch_2.phase == TouchPhase.Began) {
						startPos_2 = touch_2.position;
						Vector2 dist = touch_2.position - touch.position;
						mutual_distance = dist.magnitude;
					}

					direction = touch.position - startPos;

					Vector2 one = touch_2.position - touch.position;
					Vector2 two = startPos_2 - startPos;

					float ang = Vector2.Angle (one, two);
					Vector3 cross = Vector3.Cross (one, two);

					Vector3 worldpivot = gameObject.GetComponent<Camera>().ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, 0f));
					worldpivot.z = target.transform.position.z;

					if(ang < 10){
						if (cross.z > 0) {
							target.transform.RotateAround (worldpivot, Vector3.back, ang);
							currentrot = target.transform.eulerAngles;

							if (remote_send)
								sendTransformData();

							foreach(GameObject uni in multy){
								uni.transform.rotation = target.transform.rotation;
							}
						} else {
							target.transform.RotateAround (worldpivot, Vector3.forward, ang);
							currentrot = target.transform.eulerAngles;
						
							if (remote_send)
								sendTransformData();

							foreach(GameObject uni in multy){
								uni.transform.rotation = target.transform.rotation;
							}
						}
					}

					// Controlos de zoom
					Vector2 conector = touch_2.position - touch.position;
					distance = conector.magnitude;
					float deviance = (distance - mutual_distance)/screen_lenght;

					gameObject.GetComponent<Camera>().fieldOfView = gameObject.GetComponent<Camera>().fieldOfView  - (deviance*90);

					if (gameObject.GetComponent<Camera>().fieldOfView > 110f)
						gameObject.GetComponent<Camera>().fieldOfView = 110f;
					if (gameObject.GetComponent<Camera>().fieldOfView < 10f)
						gameObject.GetComponent<Camera>().fieldOfView = 10f;

					if (remote_send) {
						GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
						nex.SendMessage ("Send_function_fov",gameObject.GetComponent<Camera>().fieldOfView , SendMessageOptions.DontRequireReceiver);
					}
					startPos_2 = touch_2.position;
					mutual_distance = distance;
				}

				// Faz os controlos de translaçao
				if (TuioInput.touchCount > 2 && move) {

					Vector2 position = new Vector2();
					foreach(Touch toque in TuioInput.touches){
						position += toque.position;
					}
					//print(position);
					float nr = TuioInput.touchCount;
					position /= nr;
					float dist_z = gameObject.transform.position.z + initialpos.z;
					target.transform.position = gameObject.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(position.x, position.y, dist_z));

					if (remote_send)
						sendTransformData();
				}

				startPos = touch.position;
			}
		}
	}

	void Set_trans(Transform trans) {
		target.transform.position = trans.position;
		target.transform.eulerAngles = trans.eulerAngles;
		currentrot = trans.eulerAngles;
		foreach(GameObject uni in multy){
			uni.transform.eulerAngles = trans.eulerAngles;
		}
	}

	void Set_fov(float zoom) {
		gameObject.GetComponent<Camera>().fieldOfView = zoom;
	}
}