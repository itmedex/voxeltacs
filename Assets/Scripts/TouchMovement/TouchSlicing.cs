﻿using UnityEngine;
using System.Collections;

public class TouchSlicing : MonoBehaviour {

	// Face Camera Widget
	int face = 6;

	// Posiçao inicial dos dedos
	private Vector2 startPos;
	
	// Direcçao do primeiro dedo
	private Vector2 direction;

	// Id do primeiro dedo
	private bool id_set;
	private int main_finger_id;
	
	// Target
	public GameObject target;
	private Vector3 initialpos;
	private Vector3 initialrot;
	private Vector3 currentrot;
	private Vector3 initialrot_thumb;
	private Vector3 currentrot_thumb;

	//Sensibilidades
	private float sensitivity = 10;

	// Distancia entre os dedos
	private float mutual_distance;
	private float distance;

	float check_time;
	//Sensibilidades
	
	public float rot_sentivity = 1;
	public float zoom_sensitivity = 1;
	public float trans_sensitivity = 1;
	
	// Bloqueio de ecra
	public bool unlocked = true;
	
	// Screen var
	private Vector3 initialcoor;
	private Vector3 finalcoor;
	private float plane_height;
	private float plane_lenght;
	private float screen_height;
	private float screen_lenght;
	
	// Variaveis para taps
	int touches;
	
	// Rotaçao do multiview
	//private bool first = true;
	//GameObject[] multy;
	
	// Box widjet
	public CameraWidget widget;
	
	//touch camera
	private bool move= true;

	//remote
	public bool remote_send;

	// Tablet Connection Object
	GameObject tab;
	
	void Start () {

		initialpos = target.transform.position;
		initialrot = new Vector3 (target.transform.eulerAngles.x, target.transform.eulerAngles.y, target.transform.eulerAngles.z);
		currentrot = new Vector3 (target.transform.eulerAngles.x, target.transform.eulerAngles.y, target.transform.eulerAngles.z);
		initialrot_thumb = new Vector3 (0, 0, 0);
		currentrot_thumb = new Vector3 (0, 0, 0);
		tab = GameObject.FindGameObjectWithTag ("Nexus");
		screenBounds ();
		
		//first = true;

		if(widget != null)
			widget.OnCameraChanged += widget_OnCameraChanged;
	}

	void sendTransformData() {
		tab.SendMessage ("Send_function_tranform", target.transform, SendMessageOptions.DontRequireReceiver);
	}

	void sendWidgetFace(int face) {
		tab.SendMessage ("SendWidgetFace", face, SendMessageOptions.DontRequireReceiver);
	}

	void sendSliceData(float inc) {
		float[] data = new float[2];
		data[0]  = inc;
		data[1] = face;
		tab.SendMessage ("Send_function_slicing", data, SendMessageOptions.DontRequireReceiver);
	}

	// Determinar dimensoes do ecra
	void screenBounds () {
		Camera cam = GetComponent<Camera>();
		screen_height = cam.pixelHeight;
		screen_lenght = cam.pixelWidth;
	}

	void widget_OnCameraChanged(Camera camera, string view, Vector3 directionn)
	{

		if (directionn == Vector3.forward) {
			currentrot = initialrot + new Vector3 (0, 180f, 0);
			target.transform.localEulerAngles = currentrot;
			face = 1;
			if (remote_send) {
				sendTransformData();
				sendWidgetFace(face);
			}
		}
		
		if (directionn == Vector3.up) {
			currentrot = initialrot + new Vector3 (-90f, 0, 0);
			target.transform.localEulerAngles = currentrot;
			face = 2;
			if (remote_send) {
				sendTransformData();
				sendWidgetFace(face);
			}
		}
		
		if (directionn == Vector3.down) {
			currentrot = initialrot + new Vector3 (90f, 0, 0);
			target.transform.localEulerAngles = currentrot;
			face = 3;
			if (remote_send) {
				sendTransformData();
				sendWidgetFace(face);
			}
		}
		
		if (directionn == Vector3.left) {
			currentrot = initialrot + new Vector3 (0, -90f, 0);
			target.transform.localEulerAngles = currentrot;;
			face = 4;
			if (remote_send) {
				sendTransformData();
				sendWidgetFace(face);
			}
		}
		
		if (directionn == Vector3.right) {
			currentrot = initialrot + new Vector3 (0, 90f, 0);
			target.transform.localEulerAngles = currentrot;
			face = 5;
			if (remote_send) {
				sendTransformData();
				sendWidgetFace(face);
			}
		}
		
		if (directionn == Vector3.back) {
			currentrot = initialrot;
			target.transform.localEulerAngles = currentrot;
			face = 6;
			if (remote_send) {
				sendTransformData();
				sendWidgetFace(face);
			}
		}
	}

	// Update is called once per frame
	void Update () {

		if (unlocked) {

			if (TuioInput.touchCount == 0) {
				id_set = false;
				main_finger_id = 0;
			}

			if (TuioInput.touchCount > 0) {

				if (!id_set) {
					main_finger_id = TuioInput.touches [0].fingerId;
					id_set = true;
				}

				// Mudar para usar o finger id
				//Touch touch = TuioInput.touches [0];
				Touch touch = new Touch ();
				bool set = false;
				foreach (Touch touch_1 in TuioInput.touches) {
					if (touch_1.fingerId == main_finger_id) {
						touch = touch_1;
						set = true;
					}
				}
				// se nao encontrar nenhum touch com esse id, entao e porque levantou esse dedo
				// nesse caso, o primeiro dedo no vector vai ser usado
				if (!set) {
					main_finger_id = TuioInput.touches [0].fingerId;
					touch = TuioInput.touches [0];
				}

				// Limita a interacçao a uma dada zona do ecra
				move = true;
				if (touch.position.y > 0.09f * screen_height) {
					move = true;
				} else {
					move = false;
				}

				if (TuioInput.touchCount == 2 && move) {

					if (touch.phase == TouchPhase.Began) {
						startPos = touch.position;
					}

					if (touch.phase == TouchPhase.Moved) {
						float inc = (touch.position.x - startPos.x) / sensitivity;
						if (remote_send)
							sendSliceData (inc);
					}

					startPos = touch.position;
				}

			}
		} 
	}
}