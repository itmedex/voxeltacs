﻿using UnityEngine;
using System.Collections;

public class Color_Cursor_Hist2D : MonoBehaviour {

	Camera touchcamera;
	public Camera usecamera;
	private bool _mouseState;
	public GameObject Target;
	public Vector3 screenSpace;
	public Vector3 offset;
	
	// variavel touch
	private Touch touch;
	private float tap_timer;
	private int tap_count;
	
	// Objectos da janela
	public GameObject background;
	public GameObject background_2;
	
	private GameObject color_picker;
	
	private Camera color_camera;
	
	private Function_draw drawing;
	private Function_draw drawing2;
	//private Function_draw drawing3;
	//private Function_draw drawing4;

	private Hist_2D_draw drawing_canvas;
	private Custom_Function_Hist2D function;
	
	float check_time;
	int touches;
	public GameObject grapher;
	
	private TouchMovement screen;
	
	// Variaveis de interesse
	public Color selected_color;
	public bool change;

	public bool exchange;
	
	// Use this for initialization
	void Start ()
	{
		GameObject cam = GameObject.FindGameObjectWithTag ("color_picker_camera");
		Camera camera = cam.GetComponent<Camera> ();
		touchcamera = camera;
		
		GameObject back = GameObject.FindGameObjectWithTag ("color_picker_background");
		background = back;

		GameObject back_2 = GameObject.FindGameObjectWithTag ("color_picker_background_2");
		background_2 = back_2;
		
		GameObject[] touch_camera = GameObject.FindGameObjectsWithTag("touch_camera");
		Function_draw draw = touch_camera[0].GetComponent<Function_draw>();
		drawing = draw;
		draw = touch_camera[1].GetComponent<Function_draw>();
		drawing2 = draw;
		/*draw = touch_camera[2].GetComponent<Function_draw>();
		drawing3 = draw;
		draw = touch_camera[3].GetComponent<Function_draw>();
		drawing4 = draw;*/

		GameObject canvas_camera = GameObject.FindGameObjectWithTag("2dcanvas");
		drawing_canvas = canvas_camera.GetComponent<Hist_2D_draw>();

		GameObject colorgenerator = GameObject.FindGameObjectWithTag ("color_picker_camera");
		function = colorgenerator.GetComponent<Custom_Function_Hist2D>();
		
		GameObject mainscreen = GameObject.FindGameObjectWithTag ("MainCamera");
		screen = mainscreen.GetComponent<TouchMovement>();
		
		color_picker = GameObject.FindGameObjectWithTag ("color_picker");
		
		GameObject ccscreen = GameObject.FindGameObjectWithTag ("color_camera");
		color_camera = ccscreen.GetComponent<Camera> ();
		
		//getcolor();
		//function.SendMessage("Update_color", SendMessageOptions.DontRequireReceiver);



		Select ();


	}
	
	// Update is called once per frame
	void Update ()
	{
		
		if (change) {
			screen.unlocked = false;
			drawing.unlocked = false;
			drawing2.unlocked = false;
			//drawing3.unlocked = false;
			//drawing4.unlocked = false;
			drawing_canvas.unlocked = false;
			// mudar a posiçao do color picker
			Set_picker ();
			getcolor ();
			//function.SendMessage ("Update_color", SendMessageOptions.DontRequireReceiver);

		} 
		
		selected_color = GetComponent<Renderer>().material.color;
		
		
		tap_count = 0;
		
		// Debug.Log(_mouseState);
		if (TuioInput.touchCount > 0) {
			touch = TuioInput.touches [0];
		}
		
		
		
		if (touch.phase == TouchPhase.Began) {
			RaycastHit hitInfo;
			if (Target == GetClickedObject (out hitInfo)) {
				_mouseState = true;
				tap_timer = Time.time + 1f;
				screenSpace = touchcamera.WorldToScreenPoint (Target.transform.position);
				offset = Target.transform.position - touchcamera.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, screenSpace.z));
			}
			
			
		}
		if (touch.phase == TouchPhase.Ended) {
			if(_mouseState){
				Check_state();
				function.SendMessage("Update_color", SendMessageOptions.DontRequireReceiver);
				drawing.unlocked = true;
				drawing2.unlocked = true;
				//drawing3.unlocked = true;
				//drawing4.unlocked = true;
				drawing_canvas.unlocked = true;
				screen.unlocked = true;
				
				
				if(Time.time < tap_timer) {
					tap_count = 1;
				}
				
				if (tap_count == 1){
					if(change){
						Deselect();
						//screen.unlocked = true;
					}else{
						Select();
					}
				}
				
			}
			_mouseState = false;
		}
		
		if (_mouseState) {
			function.SendMessage("Update_color", SendMessageOptions.DontRequireReceiver);
			screen.unlocked = false;
			//keep track of the mouse position
			var curScreenSpace = new Vector3 (touch.position.x, touch.position.y, screenSpace.z);
			
			//convert the screen mouse position to world point and adjust with offset
			var curPosition = touchcamera.ScreenToWorldPoint (curScreenSpace) + offset;
			
			//update the position of the object in the world
			Target.transform.position = curPosition;
		}
	}
	
	
	GameObject GetClickedObject (out RaycastHit hit)
	{
		GameObject target = null;
		Ray ray = touchcamera.ScreenPointToRay (touch.position);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}
	
	private bool Check_interval(){
		
		Vector2 screen_point = touchcamera.WorldToScreenPoint (transform.position);
		
		if (screen_point.x > drawing.left && screen_point.x < drawing.right && screen_point.y > drawing.bottom && screen_point.y < drawing.top) {
			//print ("mandou mensagem para os intervalos");
			Deselect();
			background.SendMessage ("Interval_color", this.gameObject, SendMessageOptions.DontRequireReceiver);
			return true;
		} else {
			return false;
		}
		
	}
	
	public void Check_state(){

		bool int_rec = false;
		bool grad_rec = false;

		// Ver se esta sobre a recta das intensidades
		Vector3 back_pos = background.transform.position;
		
		float up = back_pos.z + (background.transform.localScale.z/2);
		float down = back_pos.z - (background.transform.localScale.z/2);
		float left = back_pos.x - (background.transform.localScale.x / 2);
		float right = back_pos.x + (background.transform.localScale.x / 2);

		// Ver se esta sobre a recta dos gradientes
		Vector3 back_pos_2 = background_2.transform.position;
		
		float up_2 = back_pos_2.z + (background_2.transform.localScale.x/2);
		float down_2 = back_pos_2.z - (background_2.transform.localScale.x/2);
		float left_2 = back_pos_2.x - (background_2.transform.localScale.z / 2);
		float right_2 = back_pos_2.x + (background_2.transform.localScale.z / 2);


		if (transform.position.x <= right + 1.5f && transform.position.x >= left - 1.5f && transform.position.z <= up && transform.position.z >= down) {
			int_rec=true;
		}

		if (transform.position.x <= right_2 && transform.position.x >= left_2 && transform.position.z <= up_2 + 0.1f && transform.position.z >= down_2 - 1.5f) {
			grad_rec=true;
		}

		if (int_rec) {
			Vector3 new_pos = transform.position;
			new_pos.z = background.transform.position.z;
			if (transform.position.x <= right + 1.5f && transform.position.x >= right) new_pos.x = right;
			if (transform.position.x >= left - 1.5f && transform.position.x <= left) new_pos.x = left;
			transform.position = new_pos;
		}

		if (grad_rec) {
			Vector3 new_pos = transform.position;
			new_pos.x = background_2.transform.position.x;
			if (transform.position.x <= up_2 + 1.5f && transform.position.x >= up_2) new_pos.z = up_2;
			if (transform.position.x >= down_2 - 1.5f && transform.position.x <= down_2) new_pos.z = down_2;
			transform.position = new_pos;
		}

		if (!int_rec && !grad_rec) {
			//print ("ver posiçoes");
			function.gameObject.SendMessage("Deselect_all", SendMessageOptions.DontRequireReceiver);
			Deselect();
			GameObject.Destroy(gameObject);
		}

		
	}
	
	
	void getcolor(){
		
		GameObject square_color = GameObject.FindGameObjectWithTag ("color_indicator");
		//Material colorsquare = square_color.GetComponent<Renderer>().material;
		Color sq_color = square_color.GetComponent<Renderer> ().material.color;
		//Criaçao da textura e atribuiçao de cor obtida pelo quadrado
		/*
		gameObject.GetComponent<Renderer>().material = colorsquare;
		selected_color = colorsquare.color;
		*/
		
		gameObject.GetComponent<Renderer> ().material.color = sq_color;
		selected_color = sq_color;
	}

	void Set_color(Color color_i){

		gameObject.GetComponent<Renderer> ().material.color = color_i;
	}
	
	void Deselect(){
		
		change = false;
		if (color_picker == null) {
			color_picker = GameObject.FindGameObjectWithTag ("color_picker");
		}
		color_picker.transform.position = new Vector3 ( -100f, -311f, -400f);
	}
	
	public void Select(){
		
		Color c_var = GetComponent<Renderer>().material.color;
		color_picker.SendMessage ("Set_cursors", c_var);
		
		GameObject[] cursores = GameObject.FindGameObjectsWithTag("color_cursor");
		
		foreach (GameObject cursor in cursores) {
			cursor.SendMessage ("Deselect", SendMessageOptions.DontRequireReceiver);
		}
		
		change = true;
		
		Set_picker ();
		getcolor ();
		function.SendMessage ("Update_color", SendMessageOptions.DontRequireReceiver);
	}
	
	void Set_picker(){
		
		Vector3 screen_coor = touchcamera.WorldToScreenPoint (transform.position);
		
		float height = touchcamera.GetComponent<Camera>().pixelHeight;
		float width = touchcamera.GetComponent<Camera>().pixelWidth;
		
		Vector3 new_screen_coor = new Vector3 ((width * 0.04f) + screen_coor.x, (height * -0.1f) + screen_coor.y, 32f);
		
		color_picker.transform.position = color_camera.ScreenToWorldPoint (new_screen_coor);
		
	}

}
