﻿using UnityEngine;
using System.Collections;

public class Custom_Function_Hist2D : MonoBehaviour {

	private GameObject colorPicker;
	private GameObject[] cursores;

	public GameObject background_intens;
	public Backgrounds_Hist2D back_1;

	public GameObject background_grad;
	public Backgrounds_Hist2D back_2;

	public RayMarching volume;
	
	private int nr_cursor;
	private int last_number;

	public int[] defined;
	public Color[] colors;

	public int[] defined_2;
	public Color[] colors_2;
	
	//Vector para a aplicaçao da cor
	public float[] applycolor;

	// Posiçoes do background de intensidades
	Vector3 back_pos;
	float left;
	float right;
	float down_1;

	// Posiçoes do background de intensidades
	Vector3 back_pos_2;
	float up;
	float down;
	
	float check_time;
	int touches;
	
	bool first = true;
	bool setup = true;

	//remote
	public bool remote_send;
	
	void Start () {

		colorPicker = GameObject.FindGameObjectWithTag("color_picker");
		
		nr_cursor = 0;
		last_number = 0;
		defined = new int[256];
		colors = new Color[256];

		// posiçoes 1
		back_pos = background_intens.transform.position;
		left = back_pos.x - (background_intens.transform.localScale.x / 2);
		right = back_pos.x + (background_intens.transform.localScale.x / 2);

		// posiçoes 2
		back_pos_2 = background_grad.transform.position;
		down = back_pos_2.z - (background_grad.transform.localScale.x / 2);
		up = back_pos_2.z + (background_grad.transform.localScale.x / 2);

		Update_color ();
		setup = false;
	}
	
	void FixedUpdate () {

		if(remote_send) {

			if (first) {
				// posiçoes 1
				back_pos = background_intens.transform.position;
				left = back_pos.x - (background_intens.transform.localScale.x / 2);
				right = back_pos.x + (background_intens.transform.localScale.x / 2);
				down_1 = back_pos.z - (background_intens.transform.localScale.z / 2);
				
				// posiçoes 2
				back_pos_2 = background_grad.transform.position;
				down = back_pos_2.z - (background_grad.transform.localScale.x / 2);
				up = back_pos_2.z + (background_grad.transform.localScale.x / 2);

				Update_color();
				first = false;
			}

			cursores = GameObject.FindGameObjectsWithTag("color_cursor");
			nr_cursor = cursores.Length;

			bool use = false;
			
			foreach (GameObject cursor in cursores) {
				Color_Cursor_Hist2D script = cursor.GetComponent<Color_Cursor_Hist2D>();
				if(script.change) use = true;
			}

			if (nr_cursor != last_number) {
				last_number = nr_cursor;
				Update_color();
			}

			if (nr_cursor == 0 || !use) {
				colorPicker.transform.position = new Vector3 ( -100f, -311f, -400f);
			}
		}
	}
	
	void Update_color(){

		defined = new int[256];
		colors = new Color[256];

		defined_2 = new int[256];
		colors_2 = new Color[256];
		
		GameObject[] cursores = GameObject.FindGameObjectsWithTag("color_cursor");
		
		if (cursores.Length != 0) {

			foreach (GameObject cursor in cursores) {

				if (cursor.transform.position.z >= down_1) {
	
					float pos = cursor.transform.position.x;
			
					int posint = Mathf.RoundToInt (((pos - left) / (right - left)) * 255);
			
					if (posint < 0)
						posint = 0;
					if (posint > 255)
						posint = 255;

					defined [posint] = 1;
					colors [posint] = cursor.GetComponent<Color_Cursor_Hist2D> ().selected_color;

				}
			
				if (cursor.transform.position.z <= up + 0.1f) {

					float pos = cursor.transform.position.z;
				
					int posint = Mathf.RoundToInt (((pos - down) / (up - down)) * 255);

					if (posint < 0)
						posint = 0;
					if (posint > 255)
						posint = 255;

					defined_2 [posint] = 1;
					colors_2 [posint] = cursor.GetComponent<Color_Cursor_Hist2D> ().selected_color;
				}
			}
		}

			// compor o vector de cores da intensidade
			if (defined [0] == 0) {
				defined [0] = 1;
				colors [0] = Color.black;
			}
			if (defined [255] == 0) {
				defined [255] = 1;
				colors [255] = Color.white;
			}
			bool begin = false;
			for (int i=0; i < defined.Length; i++) {
				if (defined [i] == 1) {
					begin = true;
				}
				if (defined [i] == 0 && begin) {
					// Encontrar proximo ponto com cor
					int j = i + 1;
					while (j < defined.Length && defined[j]==0) {
						j++;
					}
					if (j < 256) {
						//print ("lerped");
						// Interpolar linearmente as cores entre os pontos
						float fj = j;
						float fi = i;
						float t = 1 / (fj - fi + 1f);
						defined [i] = 1;
						colors [i] = Color.Lerp (colors [i - 1], colors [j], t);
					}
				}
			}

			// compor o vector de cores de gradientes
			if (defined_2 [0] == 0) {
				defined_2 [0] = 1;
				colors_2 [0] = Color.black;
			}
			if (defined_2 [255] == 0) {
				defined_2 [255] = 1;
				colors_2 [255] = Color.white;
			}
			bool begin_2 = false;
			for (int i=0; i < defined_2.Length; i++) {
				if (defined_2 [i] == 1) {
					begin_2 = true;
				}
				if (defined_2 [i] == 0 && begin_2) {
					// Encontrar proximo ponto com cor
					int j = i + 1;
					while (j < defined_2.Length && defined_2[j]==0) {
						j++;
					}
					if (j < 256) {
						//print ("lerped");
						// Interpolar linearmente as cores entre os pontos
						float fj = j;
						float fi = i;
						float t = 1 / (fj - fi + 1f);
						defined_2 [i] = 1;
						colors_2 [i] = Color.Lerp (colors_2 [i - 1], colors_2 [j], t);
					}
				}
			}
			//print ("colors updated " + cursores.Length);
			//volume.SendMessage ("Apply_color", colors,SendMessageOptions.DontRequireReceiver);
			if (!setup) {
				back_1.SendMessage ("Apply_color", SendMessageOptions.DontRequireReceiver);
				back_2.SendMessage ("Apply_color", SendMessageOptions.DontRequireReceiver);
			}

		if (remote_send) {
			float[] color_send = new float[cursores.Length*6 + 1];
			color_send[0] = cursores.Length;

			if (cursores.Length != 0) {

				int idx = 1;
				
				foreach (GameObject cursor in cursores) {
					
					if (cursor.transform.position.z >= down_1) {
						
						float pos = cursor.transform.position.x;
						
						int posint = Mathf.RoundToInt (((pos - left) / (right - left)) * 255);
						
						if (posint < 0)
							posint = 0;
						if (posint > 255)
							posint = 255;

						color_send[idx] = 1;
						color_send[idx+1] = posint;

						Color s_color = cursor.GetComponent<Color_Cursor_Hist2D> ().selected_color;
						color_send[idx+2] = s_color.r;
						color_send[idx+3] = s_color.g;
						color_send[idx+4] = s_color.b;
						color_send[idx+5] = s_color.a;
						
						idx += 6;
						
					}
					
					if (cursor.transform.position.z <= up + 0.1f) {
						
						float pos = cursor.transform.position.z;
						
						int posint = Mathf.RoundToInt (((pos - down) / (up - down)) * 255);
						
						if (posint < 0)
							posint = 0;
						if (posint > 255)
							posint = 255;
						
						color_send[idx] = 2;
						color_send[idx+1] = posint;
						
						Color s_color = cursor.GetComponent<Color_Cursor_Hist2D> ().selected_color;
						color_send[idx+2] = s_color.r;
						color_send[idx+3] = s_color.g;
						color_send[idx+4] = s_color.b;
						color_send[idx+5] = s_color.a;
						
						idx += 6;
					}
				}
			}

			GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
			nex.SendMessage ("Send_function_color_matrix", color_send, SendMessageOptions.DontRequireReceiver);
			Color_matrix ();
		} else {
			Color_matrix ();
		}
	}

	void Receive_color_update(float[] color_rec){

		defined = new int[256];
		colors = new Color[256];
		
		defined_2 = new int[256];
		colors_2 = new Color[256];

		for (int i=1; i<color_rec[0]*6 + 1; i+=6) {
			
			int idx = Mathf.RoundToInt(color_rec[i+1]);

			Color cor = Color.white;
			cor.r = color_rec[i+2];
			cor.g = color_rec[i+3];
			cor.b = color_rec[i+4];
			cor.a = color_rec[i+5];

			if(color_rec[i] == 1){
				defined[idx]=1;
				colors[idx]=cor;
			}

			if(color_rec[i] == 2){
				defined_2[idx]=1;
				colors_2[idx]=cor;
			}
		}

		// compor o vector de cores da intensidade
		if (defined [0] == 0) {
			defined [0] = 1;
			colors [0] = Color.black;
		}
		if (defined [255] == 0) {
			defined [255] = 1;
			colors [255] = Color.white;
		}
		bool begin = false;
		for (int i=0; i < defined.Length; i++) {
			if (defined [i] == 1) {
				begin = true;
			}
			if (defined [i] == 0 && begin) {
				// Encontrar proximo ponto com cor
				int j = i + 1;
				while (j < defined.Length && defined[j]==0) {
					j++;
				}
				if (j < 256) {
					//print ("lerped");
					// Interpolar linearmente as cores entre os pontos
					float fj = j;
					float fi = i;
					float t = 1 / (fj - fi + 1f);
					defined [i] = 1;
					colors [i] = Color.Lerp (colors [i - 1], colors [j], t);
				}
			}
		}
		
		// compor o vector de cores de gradientes
		if (defined_2 [0] == 0) {
			defined_2 [0] = 1;
			colors_2 [0] = Color.black;
		}
		if (defined_2 [255] == 0) {
			defined_2 [255] = 1;
			colors_2 [255] = Color.white;
		}
		bool begin_2 = false;
		for (int i=0; i < defined_2.Length; i++) {
			if (defined_2 [i] == 1) {
				begin_2 = true;
			}
			if (defined_2 [i] == 0 && begin_2) {
				// Encontrar proximo ponto com cor
				int j = i + 1;
				while (j < defined_2.Length && defined_2[j]==0) {
					j++;
				}
				if (j < 256) {

					// Interpolar linearmente as cores entre os pontos
					float fj = j;
					float fi = i;
					float t = 1 / (fj - fi + 1f);
					defined_2 [i] = 1;
					colors_2 [i] = Color.Lerp (colors_2 [i - 1], colors_2 [j], t);
				}
			}
		}

		if (!setup) {
			back_1.SendMessage ("Apply_color", SendMessageOptions.DontRequireReceiver);
			back_2.SendMessage ("Apply_color", SendMessageOptions.DontRequireReceiver);
		}
	
		Color_matrix ();
	}

	void Color_matrix(){

		Color[,] color_mat = new Color[256, 256];

		for (int i = 0; i<256; i++) {
			for (int j = 0; j<256; j++) {
				color_mat[i,j] = Color.Lerp(colors[i],colors_2[j],0.5f);
			}
		}

		volume.SendMessage ("Update_Hist_2D", color_mat ,SendMessageOptions.DontRequireReceiver);
		volume.SendMessage ("Apply_color_matrix", color_mat ,SendMessageOptions.DontRequireReceiver);
	}

	void Deselect_all(){
		GameObject[] cursores = GameObject.FindGameObjectsWithTag("color_cursor");
		foreach (GameObject cursor in cursores) {
			cursor.SendMessage ("Deselect", SendMessageOptions.DontRequireReceiver);
		}
	}

}
