﻿using UnityEngine;
using System.Collections;

public class ExampleClass : MonoBehaviour {

	Texture2D texture;

	// Recebe para desenhar pefiferia
	public Square_line perif;

	// Recebe para desenhar a area
	public Mapping mapa;
	public Histogram hist;
	public Linear_line_draw linha;

	/// <summary>
	/// Tive de inicializar a textura a cada funçao porque por alguma razao ele chamava a funçao e DEPOIS corria a
	/// a funçao start. Nao sei porque, se calhar preciso de de usar Awake, mas por agora fica assim porque funciona.
	/// 
	/// As duas primeiras funçoes servem apenas para testar as funçoes 2D. Nao tem funçao para alem do teste das mesmas.
	/// O que importa e a ultima que faz o histograma 2D. E mais uma que apaga essa imagem quando vemos o histograma 1D.
	/// </summary>

	void Start() {
	}

	void draw_perif (){

		texture = new Texture2D(256, 101);
		GetComponent<Renderer>().material.mainTexture = texture;

		int[,] periferia = perif.perif_mat;
		Color color;
		int y = 0;
		while (y < texture.height) {
			int x = 0;
			while (x < texture.width) {
				if(periferia[x,y]==1){
					color = Color.black;
				}
				else{
					color = Color.green;
				}
					texture.SetPixel(x, y, color);
				++x;
			}
			++y;
		}
		texture.Apply();

	}

	void draw_map (){

		texture = new Texture2D(256, 101);
		GetComponent<Renderer>().material.mainTexture = texture;

		int[,] area = mapa.area_map;
		Color color;

		int y = 0;
		while (y < texture.height) {
			int x = 0;
			while (x < texture.width) {
				if(area[x,y]==1){
					color = Color.black;
				}
				else{
					color = Color.green;
				}
				texture.SetPixel(x, y, color);
				++x;
			}
			++y;
		}
		texture.Apply();
		
	}

	public void draw_hist2d(){

		texture = new Texture2D(256, 101);
		GetComponent<Renderer>().material.mainTexture = texture;

		float[,] area = hist.hist2d_final;
		Color color;
		
		int y = 0;
		while (y < texture.height) {
			int x = 0;
			while (x < texture.width) {
				color = Color.white;
				color.r = 1f;
				color.g = 1f - area[x,y];
				color.b = 0f;
				texture.SetPixel(x, y, color);
				++x;
			}
			++y;
		}
		texture.Apply();
	}

	public void Reset_blank(){
		texture = new Texture2D(256, 101);
		GetComponent<Renderer>().material.mainTexture = texture;
	}
	

}