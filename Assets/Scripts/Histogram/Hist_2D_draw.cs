﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Hist_2D_draw : MonoBehaviour {

	private LineRenderer line;
	private bool isMousePressed;
	private List<Vector3> pointsList;
	private Vector3 mousePos;

	public Material line_mat;

	//
	private bool pressed;

	//
	public bool unlocked = true;

	// Screen var
	private Vector3 initialcoor;
	private Vector3 finalcoor;
	private float screen_height;
	private float screen_lenght;
	public float top;
	public float bottom;
	public float right;
	public float left;
	private float plane_height;
	private float plane_lenght;

	// Plano
	public GameObject plano;
	public GameObject plano2;
	public GameObject render_camera;

	//
	private Vector2 minimum;
	private Vector2 maximum;
	private int collision_line; 
	private bool select = true;

	// Id do primeiro dedo
	private bool id_set;
	private int main_finger_id;

	//
	public Text toggle_text;

	//remote
	public bool remote_send;

	// Structure for line points
	struct myLine
	{
		public Vector3 StartPoint;
		public Vector3 EndPoint;
	};
	//	-----------------------------------	
	void Awake()
	{
		screenBounds ();
		// Create line renderer component and set its property
		line = gameObject.AddComponent<LineRenderer>();
		//line.material =  new Material(Shader.Find("Particles/Additive"));
		line.material = line_mat;
		line.SetVertexCount(0);
		line.SetWidth(0.5f,0.5f);
		line.SetColors(Color.green, Color.green);
		line.useWorldSpace = true;	
		isMousePressed = false;
		pointsList = new List<Vector3>();
		pressed = false;

	}
	//	-----------------------------------	

	void Start(){
		//screenBounds ();
		toggle_text.text = "select";
	}

	void Update () 
	{
		screenBounds ();

		if(unlocked){

			//Touch touch = new Touch();

		// If mouse button down, remove old line and set its color to green
			if(TuioInput.touchCount == 1 && !pressed && TuioInput.touches[0].phase == TouchPhase.Began)
		{

				float xfloat = TuioInput.touches[0].position.x;
				float yfloat = TuioInput.touches[0].position.y;


				if(xfloat < right && xfloat > left && yfloat < top && yfloat > bottom){
					isMousePressed = true;
					line.SetVertexCount(0);
					pointsList.RemoveRange(0,pointsList.Count);
					if (select) {
						line.SetColors (Color.green, Color.green);
					} else {
						line.SetColors (Color.yellow, Color.yellow);
					}
					//
					pressed = true;
					minimum = new Vector2(10000,10000);
					maximum = new Vector2(-10000,-10000);
				}
		}
		else if(TuioInput.touchCount == 0)
		{
			isMousePressed = false;
			pressed=false;
				line.SetVertexCount(0);

		}
	
		// Drawing line when mouse is moving(presses)
		if(isMousePressed && pressed)
		{

				float xfloat = TuioInput.touches[0].position.x;
				float yfloat = TuioInput.touches[0].position.y;

			mousePos = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(xfloat,yfloat,0f));
			mousePos.y=-1f;
			// Verificar limites

			if (mousePos.x < initialcoor.x) {
				mousePos.x = initialcoor.x;
			}
			
			if (mousePos.z > finalcoor.z) {
				mousePos.z = finalcoor.z;
			}
			if (mousePos.x > finalcoor.x) {
				mousePos.x = finalcoor.x;
			}
			if (mousePos.z < initialcoor.z) {
				mousePos.z = initialcoor.z;
			}
			
			//

			if (!pointsList.Contains (mousePos)) 
			{
				if(mousePos.x> maximum.x) maximum.x = mousePos.x;
				if(mousePos.x< minimum.x) minimum.x = mousePos.x;
				if(mousePos.z> maximum.y) maximum.y = mousePos.z;
				if(mousePos.z< minimum.y) minimum.y = mousePos.z;

				pointsList.Add (mousePos);
				line.SetVertexCount (pointsList.Count);
				line.SetPosition (pointsList.Count - 1, (Vector3)pointsList [pointsList.Count - 1]);
				if(isLineCollide())
				{
					isMousePressed = false;
					line.SetColors(Color.red, Color.red);
//					print (collision_line);

					if(collision_line!=0){
						pointsList.RemoveRange(0,collision_line);
					}

					Define_map();
				}
			}
			}
		}
	}
	//	-----------------------------------	
	//  Following method checks is currentLine(line drawn by last two points) collided with line 
	//	-----------------------------------	
	private bool isLineCollide()
	{
		if (pointsList.Count < 2)
			return false;
		int TotalLines = pointsList.Count - 1;
		myLine[] lines = new myLine[TotalLines];
		if (TotalLines > 1) 
		{
			for (int i=0; i<TotalLines; i++) 
			{
				lines [i].StartPoint = (Vector3)pointsList [i];
				lines [i].EndPoint = (Vector3)pointsList [i + 1];
			}
		}
		for (int i=0; i<TotalLines-1; i++) 
		{
			myLine currentLine;
			currentLine.StartPoint = (Vector3)pointsList [pointsList.Count - 2];
			currentLine.EndPoint = (Vector3)pointsList [pointsList.Count - 1];
			if (isLinesIntersect (lines [i], currentLine)) 
			{
				collision_line = i;
				return true;
			}
		}
		return false;
	}
	//	-----------------------------------	
	//	Following method checks whether given two points are same or not
	//	-----------------------------------	
	private bool checkPoints (Vector3 pointA, Vector3 pointB)
	{
		return (pointA.x == pointB.x && pointA.z == pointB.z);
	}
	//	-----------------------------------	
	//	Following method checks whether given two line intersect or not
	//	-----------------------------------	
	private bool isLinesIntersect (myLine L1, myLine L2)
	{
		if (checkPoints (L1.StartPoint, L2.StartPoint) ||
		    checkPoints (L1.StartPoint, L2.EndPoint) ||
		    checkPoints (L1.EndPoint, L2.StartPoint) ||
		    checkPoints (L1.EndPoint, L2.EndPoint))
			return false;
		
		return((Mathf.Max (L1.StartPoint.x, L1.EndPoint.x) >= Mathf.Min (L2.StartPoint.x, L2.EndPoint.x)) &&
		       (Mathf.Max (L2.StartPoint.x, L2.EndPoint.x) >= Mathf.Min (L1.StartPoint.x, L1.EndPoint.x)) &&
		       (Mathf.Max (L1.StartPoint.z, L1.EndPoint.z) >= Mathf.Min (L2.StartPoint.z, L2.EndPoint.z)) &&
		       (Mathf.Max (L2.StartPoint.z, L2.EndPoint.z) >= Mathf.Min (L1.StartPoint.z, L1.EndPoint.z)) 
		       );
	}

	void screenBounds () {
		
		Camera cam = GetComponent<Camera>();
		
		Vector3 screenPosfi = cam.WorldToScreenPoint(transform.position);
		
		Vector3 centralpixel = screenPosfi;
		
		screen_height = cam.pixelHeight;
		screen_lenght = cam.pixelWidth;
		
		top = centralpixel.y + screen_height / 2;
		bottom = centralpixel.y - screen_height / 2;
		left = centralpixel.x - screen_lenght / 2;
		right = centralpixel.x + screen_lenght / 2;
		
		initialcoor = cam.ScreenToWorldPoint (new Vector3 (left, bottom, 0f));
		finalcoor = cam.ScreenToWorldPoint (new Vector3 (right, top, 0f));

		//print (initialcoor);
		//print (finalcoor);

		plane_height = finalcoor.z - initialcoor.z;
		plane_lenght = finalcoor.x - initialcoor.x;
		
		Vector3 scale = new Vector3(0,0,0);
		
		scale.x = plane_lenght;
		scale.y = plane_height;
		scale.z = 0.01f;
		
		plano.transform.localScale = scale;
		plano2.transform.localScale = scale;
	}

	void Define_map(){

		int[,] map = new int[256, 256];

		float full_increment_x = plane_lenght / 256f;
		float full_increment_y = plane_height / 256f;

		float offset_x = full_increment_x / 2f;
		float offset_y = full_increment_y / 2f;

		float xcent = minimum.x - initialcoor.x;
		float xescal = (xcent / plane_lenght) * 255;
		int x_min = Mathf.RoundToInt (xescal);

		float ycent = minimum.y - initialcoor.z;
		float yescal = (ycent / plane_height) * 255;
		int y_min = Mathf.RoundToInt (yescal);

		float xcent_2 = maximum.x - initialcoor.x;
		float xescal_2 = (xcent_2 / plane_lenght) * 255;
		//int x_max = Mathf.RoundToInt (xescal_2);
		
		float ycent_2 = maximum.y - initialcoor.z;
		float yescal_2 = (ycent_2 / plane_height) * 255;
		//int y_max = Mathf.RoundToInt (yescal_2);

		Vector2[] pointsArray = new Vector2[pointsList.Count];

		for(int i =0; i<pointsList.Count; i++){
			Vector2 pos = new Vector2(0,0);
			pos.x = pointsList[i].x;
			pos.y = pointsList[i].z;
			pointsArray[i]=pos;
		}

		if (remote_send) {

			Vector2[] points_send = new Vector2[pointsArray.Length + 2];

			int idx = 2;

			points_send[0].x = pointsArray.Length;

			if(select){
				points_send[0].y = 1;
			}else{
				points_send[0].y = 2;
			}

			//points_send[1].x = minimum;
			//points_send[1].y = maximum;

			foreach( Vector2 point in pointsArray){

				// Centrar as coordenadas do cursor
				float xcent_c = point.x - initialcoor.x;
				float ycent_c = point.y - initialcoor.z;
			
			
				// Escalar valores de x
				float xescal_c = (xcent_c / plane_lenght) * 255;
				// Valor escalado inteiro de x
				int xescalint = Mathf.RoundToInt (xescal_c);
				//print (xescalint);
			
				// escalar valores de y
				float yescal_c = (ycent_c / plane_height) * 255;
				// Valor escalado inteiro de y
				int yesclint = Mathf.RoundToInt (yescal_c);

				points_send[idx].x = xescalint;
				points_send[idx].y = yesclint;

				idx++;
			}

			GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
			nex.SendMessage ("Send_opac_map", points_send, SendMessageOptions.DontRequireReceiver);
			//Resend_lines (points_send);
		}

		for (int i = x_min; i<xescal_2 + 1f; i++) {
			for (int j = y_min; j<yescal_2 + 1f; j++) {

				float space_x = initialcoor.x + offset_x + full_increment_x*i;
				float space_y = initialcoor.z + offset_y + full_increment_y*j;

				Vector2 space_pos = new Vector2(space_x,space_y);

				if(IsPointInPolygon(pointsArray,space_pos)){
					if(select){
						map[i,j]=1;
					}else{
						map[i,j]=2;
					}
				}
			}
		}
			//render_camera.SendMessage("Paint_map", map , SendMessageOptions.DontRequireReceiver);
			render_camera.SendMessage("Apply_opac_map", map , SendMessageOptions.DontRequireReceiver);

	}

	void Resend_lines (Vector2[] pointsvector){

		int[,] map = new int[256, 256];

		//print("resend");
		/*
		minimum = pointsvector [1].x;
		maximum = pointsvector [1].y;
		
		float full_increment_x = plane_lenght / 256f;
		float full_increment_y = plane_height / 256f;
		
		float offset_x = full_increment_x / 2f;
		float offset_y = full_increment_y / 2f;
		
		float xcent = minimum.x - initialcoor.x;
		float xescal = (xcent / plane_lenght) * 255;
		int x_min = Mathf.RoundToInt (xescal);
		
		float ycent = minimum.y - initialcoor.z;
		float yescal = (ycent / plane_height) * 255;
		int y_min = Mathf.RoundToInt (yescal);
		
		float xcent_2 = maximum.x - initialcoor.x;
		float xescal_2 = (xcent_2 / plane_lenght) * 255;
		
		float ycent_2 = maximum.y - initialcoor.z;
		float yescal_2 = (ycent_2 / plane_height) * 255;


		for (int i = x_min; i<xescal_2 + 1f; i++) {
			for (int j = y_min; j<yescal_2 + 1f; j++) {
				
				float space_x = initialcoor.x + offset_x + full_increment_x*i;
				float space_y = initialcoor.z + offset_y + full_increment_y*j;
				
				Vector2 space_pos = new Vector2(space_x,space_y);
				
				if(IsPointInPolygon(pointsvector,space_pos)){
					//print(i + " " + j);
					if(select){
						map[i,j]=1;
					}else{
						map[i,j]=2;
					}
				}
			}
		}
		*/

		Vector2[] circunf = new Vector2[pointsvector.Length - 2];

		for (int i=2; i<pointsvector.Length; i++) {
			circunf[i-2] = pointsvector[i];
		}

		for (int i = 0; i<255; i++) {
			for (int j = 0; j<255; j++) {

				if(IsPointInPolygon(circunf,new Vector2(i,j))){
					if(pointsvector[0].y == 1){
						map[i,j]=1;
					}else{
						map[i,j]=2;
					}
				}
			}
		}

		//render_camera.SendMessage("Paint_map", map , SendMessageOptions.DontRequireReceiver);
		render_camera.SendMessage("Apply_opac_map", map , SendMessageOptions.DontRequireReceiver);
	}

	private bool IsPointInPolygon(Vector2[] polygon, Vector2 point)
	{
		bool isInside = false;
		for (int i = 0, j = polygon.Length - 1; i < polygon.Length; j = i++)
		{
			if (((polygon[i].y > point.y) != (polygon[j].y > point.y)) &&
			    (point.x < (polygon[j].x - polygon[i].x) * (point.y - polygon[i].y) / (polygon[j].y - polygon[i].y) + polygon[i].x))
			{
				isInside = !isInside;
			}
		}
		return isInside;
	}

	public void Toggle_function(){
		select = !select;
		if(select) toggle_text.text = "select";
		if(!select) toggle_text.text = "deselect";
	}
}
