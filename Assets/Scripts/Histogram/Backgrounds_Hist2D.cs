﻿using UnityEngine;
using System.Collections;

public class Backgrounds_Hist2D : MonoBehaviour {
	
	Camera touchcamera;
	public Camera usecamera;
	private bool _mouseState;
	public GameObject Target;
	
	// variavel touch
	private Touch touch;
	
	// 
	float check_time;
	int touches;
	public Custom_Function_Hist2D function;
	
	// Textura para o background
	private Texture2D barra_cores;
	private Color[] grey_scale;
	private int[] defined;
	private Color[] colors;

	//
	public bool intens;

	public Vector3 leftpoint;
	public Vector3 rightpoint;
	
	void Start ()
	{
		touchcamera = usecamera;

		if (intens) {
			Set_length ();
			Set_point_y();
		} else {
			Set_height();
			Set_point_x();
		}

		Generate_texture ();
	}
	
	void Update ()
	{	
		// Debug.Log(_mouseState);
		if (TuioInput.touchCount > 0) {
			touch = TuioInput.touches [0];
		}
		
		if (touch.phase == TouchPhase.Began) {
			RaycastHit hitInfo;
			if (Target == GetClickedObject (out hitInfo)) {
				_mouseState = true;
				
				// Ve toques e instancia um novo cursor
				if (Time.time > check_time + 0.5f){
					touches = 1;
					check_time = Time.time;
				}else{
					touches = touches + 1;
					check_time = Time.time;
				}
				
				if (touches == 2){
					//print ("instanciate");
					GameObject cursor = (GameObject)Instantiate (Resources.Load ("color_cursor_hist2d"));
					Vector3 newpos = touchcamera.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, 0f));
					Vector3 pos = cursor.transform.position;
					pos.x = newpos.x;
					pos.z = newpos.z;
					cursor.transform.position = pos;
					//cursor.SendMessage("Check_state", SendMessageOptions.DontRequireReceiver);
					//function.SendMessage("Update_color", SendMessageOptions.DontRequireReceiver);
				}
			}
		}
		if (touch.phase == TouchPhase.Ended) {
			_mouseState = false;
		}
	}
	
	GameObject GetClickedObject (out RaycastHit hit)
	{
		GameObject target = null;
		Ray ray = touchcamera.ScreenPointToRay (touch.position);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}
	
	void Set_length(){
		
		float length = touchcamera.pixelWidth;
		float leftpixel = 0.35f * length;
		float rightpixel = 0.65f * length;
		
		leftpoint = touchcamera.ScreenToWorldPoint (new Vector3 (leftpixel, 0f, 0f));
		rightpoint = touchcamera.ScreenToWorldPoint (new Vector3 (rightpixel, 0f, 0f));
		
		float back_length = rightpoint.x - leftpoint.x;
		
		Vector3 scale = transform.localScale;
		scale.x = back_length;
		transform.localScale = scale;
	}

	void Set_height(){
		
		float height = touchcamera.pixelHeight;
		float lowerpixel = 0.09f * height;
		float higherpixel = 0.32f * height;
		
		Vector3 lowerpoint = touchcamera.ScreenToWorldPoint (new Vector3 (0f, lowerpixel, 0f));
		Vector3 higherpoint = touchcamera.ScreenToWorldPoint (new Vector3 (0f, higherpixel, 0f));
		
		float back_height = higherpoint.z - lowerpoint.z;

		Vector3 scale = transform.localScale;
		scale.x = back_height;
		transform.localScale = scale;
	}

	void Set_point_x(){

		float length = touchcamera.pixelWidth;
		float point_pixel = 0.66f * length;
		
		Vector3 pos_point = touchcamera.ScreenToWorldPoint (new Vector3 (point_pixel, 0f, 0f));

		Vector3 world_pos = transform.position;
		world_pos.x = pos_point.x; 
		transform.position = world_pos;
	}

	void Set_point_y(){
		
		float height = touchcamera.pixelHeight;
		float point_pixel = 0.34f * height;
		
		Vector3 pos_point = touchcamera.ScreenToWorldPoint (new Vector3 (0f, point_pixel, 0f));
		
		Vector3 world_pos = transform.position;
		world_pos.z = pos_point.z; 
		transform.position = world_pos;
	}
	
	void Generate_texture(){
		
		barra_cores = new Texture2D (256, 1);
		
		Color color_0 = Color.black;
		Color color_1 = Color.white;

		if (!intens) {
			color_0 = Color.black;
			color_1 = Color.white;
		}
		
		grey_scale = new Color[256];
		
		for (int i=0; i<grey_scale.Length; i++) {
			float nr = i;
			float percent = nr / 255f;
			grey_scale[i] = Color.Lerp(color_0, color_1, percent);
			barra_cores.SetPixel(255 - i, 0, grey_scale[i]);
		}
		
		barra_cores.Apply ();
		GetComponent<Renderer>().material.mainTexture = barra_cores;
	}
	
	void Apply_color(){

		if (intens) {
			defined = function.defined;
			colors = function.colors;
		} else {
			defined = function.defined_2;
			colors = function.colors_2;
		}
		
		for (int i=0; i<defined.Length; i++) {
			
			if(defined[i]==1){
				barra_cores.SetPixel(255-i, 0, colors[i]);
			}else{
				barra_cores.SetPixel(255-i, 0, Color.white);
			}
		}
		barra_cores.Apply ();
	}
	
	public void Interval_color(GameObject cursor){
		
		//print ("interval color");
		
		float intervalo = transform.localScale.x / 10f;
		
		for (int i = 0; i<10; i++) {
			float one = leftpoint.x + intervalo*i;
			float two = leftpoint.x + intervalo*(i+1);
			
			if(cursor.transform.position.x >= one && cursor.transform.position.x < two){
				
				Vector3 pos_one = cursor.transform.position;
				pos_one.x = one;
				pos_one.z = transform.position.z;
				
				GameObject cursor_1 = (GameObject)Instantiate (Resources.Load ("color_cursor_hist2d"));
				cursor_1.GetComponent<Renderer>().material.color = cursor.GetComponent<Renderer>().material.color;
				cursor_1.transform.position = pos_one;
				
				Vector3 pos_two = cursor.transform.position;
				pos_two.x = two;
				pos_two.z = transform.position.z;
				
				GameObject cursor_2 = (GameObject)Instantiate (Resources.Load ("color_cursor_hist2d"));
				cursor_2.GetComponent<Renderer>().material.color = cursor.GetComponent<Renderer>().material.color;
				cursor_2.transform.position = pos_two;
			}
		}
		
		GameObject.Destroy (cursor);
		function.SendMessage("Update_color", SendMessageOptions.DontRequireReceiver);
		
	}
}

