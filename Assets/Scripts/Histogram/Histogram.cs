﻿using UnityEngine;
using System.Collections;

public class Histogram : MonoBehaviour {

	// Camera variables
	Camera touchcamera;
	public Camera usecamera;
	public Touchscript touchscript;

	// Normalization measurementes
	Vector3 centralpixel;
	Vector3 initialcoor;
	Vector3 finalcoor;
	
	// Medidas do plano e camara para normalizaçao
	float screen_lenght;
	float screen_height;
	float plane_lenght;
	float plane_height;

	// Fonte do histograma
	//public Grapher5Up fonte;
	private float[] histograma;

	// Fonte do histograma 2D
	private Color[,,] cores;
	private int[,,] gradientes;

	// Objecto do histograma
	private GameObject histline;
	[Range(0,256)]
	public float histpoints = 100;

	// Objecto do histograma 2D
	private float[,] hist2d_count;
	public float[,] hist2d_final;
	[Range(1,10)]
	public float potencia;
	private float correntpotencia;
	private bool iniciado;

	// Medidas da imagem
	private int fatias;
	private int largura;
	private int altura;
	
	// Manda desenhar a area do mapa
	public ExampleClass desenho;
	public bool one_dimension;

	void Create_Histo(float[] hist){

		touchcamera = usecamera;

		screenBounds ();

		histograma = hist;

		// Normalizar
		histograma = Normalize (histograma);

		float interval = plane_lenght / (histpoints-1);

		for (int j=0; j < histpoints; j++) {

			histline = (GameObject)Instantiate(Resources.Load("Histogram_cube"));

			Vector3 worldPos;
			worldPos.x = initialcoor.x + (j*interval);
			worldPos.z = initialcoor.z;
			worldPos.y = 2f;

			histline.transform.position = worldPos;

			// Centrar posiçao de x
			float xcent = histline.transform.position.x - initialcoor.x;
			// Escalar valores de x
			float xescal = (xcent/plane_lenght)* 255;
			// Valor escalado inteiro de x
			int histpos = Mathf.RoundToInt(xescal);

			Vector3 worldScale;
			worldScale.x = 2 * interval;
			worldScale.z = 2 * plane_height * histograma[histpos];
			worldScale.y = 0.2f;

			histline.transform.localScale = worldScale;
		}

	}


	void Create_Histo_Grad(float[] hist){
		
		touchcamera = usecamera;
		
		screenBounds ();
		
		histograma = hist;
		
		// Normalizar
		histograma = Normalize (histograma);
		
		float interval = plane_lenght / (histpoints-1);
		
		for (int j=0; j < histpoints; j++) {
			
			histline = (GameObject)Instantiate(Resources.Load("Histogram_cube"));
			
			Vector3 worldPos;
			worldPos.x = initialcoor.x + (j*interval);
			worldPos.z = initialcoor.z;
			worldPos.y = 2f;
			
			histline.transform.position = worldPos;
			
			// Centrar posiçao de x
			float xcent = histline.transform.position.x - initialcoor.x;
			// Escalar valores de x
			float xescal = (xcent/plane_lenght)* 255;
			// Valor escalado inteiro de x
			int histpos = Mathf.RoundToInt(xescal);
			
			Vector3 worldScale;
			worldScale.x = 2 * interval;
			worldScale.z = 2 * plane_height * histograma[histpos];
			worldScale.y = 0.2f;
			
			histline.transform.localScale = worldScale;
		}
		
	}

	/*
	public void Create_Histo2D(){

		touchcamera = usecamera;
		iniciado = true;

		hist2d_count = new float[256,101];

		fatias = fonte.fatias;
		largura = fonte.largura;
		altura = fonte.altura;

		cores = fonte.cores;
		gradientes = fonte.grad_scal_int;

		for (int z = 0; z < fatias; z++) {
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					int intval = Mathf.RoundToInt(GetIntens(x,y,z)*255);
					int gradval = gradientes[x,y,z];
					hist2d_count[intval,gradval] = hist2d_count[intval,gradval] + 1f;
				}
			}
		}

		float max = 0;
		for (int x = 0; x < 256; x++) {
			for (int y = 0; y < 101 ; y++) {
				if (hist2d_count[x,y] > max){
					max = hist2d_count[x,y];
				}
			}
		}
		//print ("o maximo e " + max);
		for (int x = 0; x < 256; x++) {
			for (int y = 0; y < 101 ; y++) {
				hist2d_count[x,y] = (hist2d_count[x,y] *Mathf.Pow(10, potencia)) / max; 
			}
		}

		hist2d_final = hist2d_count;
		//print ("histograma feito");
		//hist2d.SendMessage("CreatePoints2D", SendMessageOptions.DontRequireReceiver);

	
			desenho.SendMessage ("draw_hist2d", SendMessageOptions.DontRequireReceiver);
	}
	*/

	float[] Normalize (float[] histograma){

		float max = 0;
		float[] hist = histograma;

		for (int i=0; i < hist.Length; i++) {
			if(hist[i]>max){
				max = hist[i];
			}
		}

		for (int i=0; i < hist.Length; i++) {
			hist[i] = hist[i]/max;
		}
	
		return hist;
	}

	

	void screenBounds () {
		
		Vector3 screenPosfi = touchcamera.WorldToScreenPoint(touchcamera.transform.position);
		
		centralpixel = screenPosfi;
		
		screen_height = touchcamera.pixelHeight;
		screen_lenght = touchcamera.pixelWidth;
		
		initialcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x - screen_lenght / 2, centralpixel.y - screen_height / 2, 0f));
		finalcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x + screen_lenght / 2, centralpixel.y + screen_height / 2, 0f));
		
		plane_height = finalcoor.z - initialcoor.z;
		plane_lenght = finalcoor.x - initialcoor.x;
	
	}

	float GetIntens ( int x, int y, int z){
		
		// Corrigir coordenadas
		
		int x_1 = x;
		int y_1 = y;
		int z_1 = z;
		
		if (x < 0) {
			x_1 = 0;
		}
		
		if (x >= largura) {
			x_1 = largura-1;
		}
		
		if (y < 0) {
			y_1 = 0;
		}
		
		if (y >= altura) {
			y_1 = altura-1;
		}
		
		if (z < 0) {
			z_1 = 0;
		}
		
		if (z >= fatias) {
			z_1 = fatias-1;
		}
		
		// determinar intesidade
		Color co = cores [x_1, y_1, z_1];
		
		float co_intens = ((co.r + co.b + co.g) / 3);
		
		return co_intens;
	}


}
