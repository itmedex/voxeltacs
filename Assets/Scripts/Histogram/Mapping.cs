﻿using UnityEngine;
using System.Collections;

public class Mapping : MonoBehaviour {


	// Receber o perimetro
	public Cursor track;
	public Square_line square;
	public Square_line_fixed square_fixo;
	private int[,] mapa;
	public int[,] area_map;

	// Manda desenhar o sitema de particulas
//	public Grapher5Up SP;

	// Manda desenhar a area do mapa
	public ExampleClass desenho;

	void mapear (){

		mapa = track.transf2d;

		for (int x= 0; x < 256; x++) {
			for(int y=0; y < 101; y++){

				int count = 0;
				// Verifica se e zero
				if (mapa[x,y]==0){

					for (int i=x+1; i < 256; i++){
						if(mapa[i,y]==1){
							count = count + 1;
							i=256;
						}
					}

					for (int i=x-1; i > 0; i--){
						if(mapa[i,y]==1){
							count = count + 1;
							i=0;
						}
					}

					for (int i=y+1; i < 101; i++){
						if(mapa[x,i]==1){
							count = count +1;
							i = 101;
						}
					}
					
					for (int i=y-1; i > 0; i--){
						if(mapa[x,i]==1){
							count = count + 1;
							i=0;
						}
					}
				}

				if(count==4){
					mapa[x,y]=1;
				}
			}
		}

		area_map = mapa;
		//desenho.SendMessage ("draw_map", SendMessageOptions.DontRequireReceiver);
		//SP.SendMessage ("ManualFunc2D", SendMessageOptions.DontRequireReceiver);
	}

	void mapear_square (){
		
		mapa = square.perif_mat;
		
		for (int x= 0; x < 256; x++) {
			for(int y=0; y < 101; y++){
				
				int count = 0;
				// Verifica se e zero
				if (mapa[x,y]==0){
					
					for (int i=x+1; i < 256; i++){
						if(mapa[i,y]==1){
							count = count + 1;
							i=256;
						}
					}
					
					for (int i=x-1; i > 0; i--){
						if(mapa[i,y]==1){
							count = count + 1;
							i=0;
						}
					}
					
					for (int i=y+1; i < 101; i++){
						if(mapa[x,i]==1){
							count = count +1;
							i = 101;
						}
					}
					
					for (int i=y-1; i > 0; i--){
						if(mapa[x,i]==1){
							count = count + 1;
							i=0;
						}
					}
				}
				
				if(count==4){
					mapa[x,y]=1;
				}
			}
		}

		
		area_map = mapa;
		//desenho.SendMessage ("draw_map", SendMessageOptions.DontRequireReceiver);
		//SP.SendMessage ("ManualFunc2D", SendMessageOptions.DontRequireReceiver);
	}

	void mapear_square_fixo (){
		
		mapa = square_fixo.perif_mat;
		
		for (int x= 0; x < 256; x++) {
			for(int y=0; y < 101; y++){
				
				int count = 0;
				// Verifica se e zero
				if (mapa[x,y]==0){
					
					for (int i=x+1; i < 256; i++){
						if(mapa[i,y]==1){
							count = count + 1;
							i=256;
						}
					}
					
					for (int i=x-1; i > 0; i--){
						if(mapa[i,y]==1){
							count = count + 1;
							i=0;
						}
					}
					
					for (int i=y+1; i < 101; i++){
						if(mapa[x,i]==1){
							count = count +1;
							i = 101;
						}
					}
					
					for (int i=y-1; i > 0; i--){
						if(mapa[x,i]==1){
							count = count + 1;
							i=0;
						}
					}
				}
				
				if(count==4){
					mapa[x,y]=1;
				}
			}
		}
		
		
		area_map = mapa;
		//desenho.SendMessage ("draw_map", SendMessageOptions.DontRequireReceiver);
		//SP.SendMessage ("ManualFunc2D", SendMessageOptions.DontRequireReceiver);
	}


}
