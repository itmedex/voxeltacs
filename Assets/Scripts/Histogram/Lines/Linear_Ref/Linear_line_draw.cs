﻿using UnityEngine;
using System.Collections;

public class Linear_line_draw : MonoBehaviour {


	// Line Variables - Line renderer
	public GameObject lines;
	private LineRenderer path;
	private int i;
	
	// Vertices
	public GameObject Ref_1;
	public GameObject Ref_2;
	public GameObject Ref_3;
	public GameObject Ref_4;
	public GameObject Ref_5;
	public GameObject Ref_6;
	public GameObject Ref_7;
	public GameObject Ref_8;
	public GameObject Ref_9;

	public GameObject[] Refs;

	// Origem das medidas do quadro
	public Cursor cursor;
	
	// Vector da Funçao de transferencia
	private float[] Func_line;
	public float[] Func_trans;

	// Sistema de particulas
	//public Grapher5Up alvo;

	
	// Projecta para o plano para testar - retirar quando funcionar bem
	public ExampleClass plano;
	
	void Start () {

		Refs = new GameObject[9];

		Refs [0] = Ref_1;
		Refs [1] = Ref_2;
		Refs [2] = Ref_3;
		Refs [3] = Ref_4;
		Refs [4] = Ref_5;
		Refs [5] = Ref_6;
		Refs [6] = Ref_7;
		Refs [7] = Ref_8;
		Refs [8] = Ref_9;

		i = Refs.Length;

		path = lines.GetComponent<LineRenderer> ();
		path.SetVertexCount (i);
		path.SetPosition (0, Ref_1.transform.position);
		path.SetPosition (1, Ref_2.transform.position);
		path.SetPosition (2, Ref_3.transform.position);
		path.SetPosition (3, Ref_4.transform.position);
		path.SetPosition (4, Ref_5.transform.position);
		path.SetPosition (5, Ref_6.transform.position);
		path.SetPosition (6, Ref_7.transform.position);
		path.SetPosition (7, Ref_8.transform.position);
		path.SetPosition (8, Ref_9.transform.position);
	}
	
	void Update () {
		
		path.SetPosition (0, Ref_1.transform.position);
		path.SetPosition (1, Ref_2.transform.position);
		path.SetPosition (2, Ref_3.transform.position);
		path.SetPosition (3, Ref_4.transform.position);
		path.SetPosition (4, Ref_5.transform.position);
		path.SetPosition (5, Ref_6.transform.position);
		path.SetPosition (6, Ref_7.transform.position);
		path.SetPosition (7, Ref_8.transform.position);
		path.SetPosition (8, Ref_9.transform.position);
		
		Make_Line ();

		Func_trans = Func_line;

		//plano.SendMessage ("draw_line", SendMessageOptions.DontRequireReceiver);
	//	alvo.SendMessage ("Linear_Func", SendMessageOptions.DontRequireReceiver);
		
		
	}
	
	void Make_Line(){
		
		///// Criar a linha
		
		Func_line = new float[256];

		for (int i=0; i < Refs.Length-1; i++) {

			GameObject Left = Refs[i];
			GameObject Right = Refs[i+1];

			// centrar coordenadas dos pontos da esquerda
			float xcent_L = Left.transform.position.x-cursor.initialcoor.x;
			float ycent_L = Left.transform.position.z-cursor.initialcoor.z;

			// centrar coordenadas dos pontos da direita
			float xcent_R = Right.transform.position.x-cursor.initialcoor.x;
			float ycent_R = Right.transform.position.z-cursor.initialcoor.z;

			// escalar valores da esquerda
			float xescal_L = (xcent_L/cursor.plane_lenght)* 255;
			float yescal_L = (ycent_L/cursor.plane_height);

			// Valores escalados inteiros da esquerda
			int xescalint_L = Mathf.RoundToInt(xescal_L);
		//	int yescalint_L = Mathf.RoundToInt(yescal_L);

			// escalar valores da esquerda
			float xescal_R = (xcent_R/cursor.plane_lenght)* 255;
			float yescal_R = (ycent_R/cursor.plane_height);

			// Valores escalados inteiros da direita
			int xescalint_R = Mathf.RoundToInt(xescal_R);
//			int yescalint_R = Mathf.RoundToInt(yescal_R);

			// Marcar os pontos de referencia no vector

			Func_line[xescalint_L] = yescal_L;
			Func_line[xescalint_R] = yescal_R;


			float y_prev;
			float declive;
			// Definir linha superior que une os pontos
			y_prev = yescal_L;
			declive = Declive (xescal_L, yescal_L, xescal_R, yescal_R);
			for (int x = xescalint_L + 1; x < xescalint_R; x++) {
				float yf;
				yf = y_prev + declive;
				//int y_int = Mathf.RoundToInt(yf);
				Func_line[x]=yf;
				y_prev = yf;
			}

		}

	}
	
	float Declive(float x_e, float y_e, float x_d, float y_d ){
		float larg = Mathf.Abs(x_d - x_e);
		float alt =  y_d - y_e;
		float decl = alt / larg;
		return decl;
	}
}
