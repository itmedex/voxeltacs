﻿using UnityEngine;
using System.Collections;

public class Square_line : MonoBehaviour {


	// Line Variables
	public GameObject lines;
	private LineRenderer path;
	private int i;

	// Vertices
	public GameObject SE;
	public GameObject SD;
	public GameObject IE;
	public GameObject ID;

	// Origem das medidas do quadro
	public Cursor cursor;

	// Matrix de periferia
	public int[,] perif_mat;

	// Manda preencher a matriz
	public Mapping mapa;

	// Projecta para o plano para testar - retirar quando funcionar bem
	public ExampleClass plano;

	void Start () {

		i = 5;

		path = lines.GetComponent<LineRenderer> ();
		path.SetVertexCount (i);
		path.SetPosition(0, SE.transform.position);
		path.SetPosition(1, SD.transform.position);
		path.SetPosition(2, ID.transform.position);
		path.SetPosition(3, IE.transform.position);
		path.SetPosition(4, SE.transform.position);
	
	}

	void Update () {

		path.SetPosition(0, SE.transform.position);
		path.SetPosition(1, SD.transform.position);
		path.SetPosition(2, ID.transform.position);
		path.SetPosition(3, IE.transform.position);
		path.SetPosition(4, SE.transform.position);
	
		Make_perif ();

		//plano.SendMessage ("draw_perif", SendMessageOptions.DontRequireReceiver);
		mapa.SendMessage ("mapear_square", SendMessageOptions.DontRequireReceiver);


	}

	void Make_perif(){

		///// Criar a matrix

		perif_mat = new int[256, 101];

		///// Centrar e escalar os quatro pontos

		// Centrar as coordenadas dos pontos
		//SE
		float xcent_SE = SE.transform.position.x-cursor.initialcoor.x;
		float ycent_SE = SE.transform.position.z-cursor.initialcoor.z;
		//SD
		float xcent_SD = SD.transform.position.x-cursor.initialcoor.x;
		float ycent_SD = SD.transform.position.z-cursor.initialcoor.z;
		//ID
		float xcent_ID = ID.transform.position.x-cursor.initialcoor.x;
		float ycent_ID = ID.transform.position.z-cursor.initialcoor.z;
		//IE
		float xcent_IE = IE.transform.position.x-cursor.initialcoor.x;
		float ycent_IE = IE.transform.position.z-cursor.initialcoor.z;
		
		// Escalar valores de x
		float xescal_SE = (xcent_SE/cursor.plane_lenght)* 255;
		float xescal_SD = (xcent_SD/cursor.plane_lenght)* 255;
		float xescal_ID = (xcent_ID/cursor.plane_lenght)* 255;
		float xescal_IE = (xcent_IE/cursor.plane_lenght)* 255;
		// Valor escalado inteiro de x
		int xescalint_SE = Mathf.RoundToInt(xescal_SE);
		int xescalint_SD = Mathf.RoundToInt(xescal_SD);
		int xescalint_ID = Mathf.RoundToInt(xescal_ID);
		int xescalint_IE = Mathf.RoundToInt(xescal_IE);
		
		// Escalar valores de x
		float yescal_SE = (ycent_SE/cursor.plane_height)* 100;
		float yescal_SD = (ycent_SD/cursor.plane_height)* 100;
		float yescal_ID = (ycent_ID/cursor.plane_height)* 100;
		float yescal_IE = (ycent_IE/cursor.plane_height)* 100;
		// Valor escalado inteiro de x
		int yescalint_SE = Mathf.RoundToInt(yescal_SE);
		int yescalint_SD = Mathf.RoundToInt(yescal_SD);
		int yescalint_ID = Mathf.RoundToInt(yescal_ID);
		int yescalint_IE = Mathf.RoundToInt(yescal_IE);

		///// Definir os pontos na matrix
		perif_mat [xescalint_SE, yescalint_SE] = 1;
		perif_mat [xescalint_SD, yescalint_SD] = 1;
		perif_mat [xescalint_ID, yescalint_ID] = 1;
		perif_mat [xescalint_IE, yescalint_IE] = 1;

		float x_prev;
		float y_prev;
		float declive;

		///// Definir linha superior - entre SE e SD
		x_prev = xescalint_SE;
		y_prev = yescalint_SE;
		declive = Declive (xescal_SE, yescal_SE, xescal_SD, yescal_SD);
		for (int x = xescalint_SE + 1; x < xescalint_SD; x++) {
			float yf;
			yf = y_prev + declive;
			int y_int = Mathf.RoundToInt(yf);
			perif_mat[x,y_int] = 1;
			y_prev = yf;
		}

		///// Definir linha direira - entre SD e ID
		x_prev = xescalint_SD;
		y_prev = yescalint_SD;
		declive = Declive (yescal_SD, xescal_SD, yescal_ID, xescal_ID);
		for (int y = yescalint_SD - 1; y > yescalint_ID; y--) {
			float xf;
			xf = x_prev + declive;
			int x_int = Mathf.RoundToInt(xf);
			perif_mat[x_int,y] = 1;
			x_prev = xf;
		}

		///// Definir linha inferior - entre IE e ID
		x_prev = xescalint_IE;
		y_prev = yescalint_IE;
		declive = Declive (xescal_IE, yescal_IE, xescal_ID, yescal_ID);
		for (int x = xescalint_IE + 1; x < xescalint_ID; x++) {
			float yf;
			yf = y_prev + declive;
			int y_int = Mathf.RoundToInt(yf);
			perif_mat[x,y_int] = 1;
			y_prev = yf;
		}

		///// Definir linha esquerda - entre SE e IE
		x_prev = xescalint_SE;
		y_prev = yescalint_SE;
		declive = Declive (yescal_SE, xescal_SE, yescal_IE, xescal_IE);
		for (int y = yescalint_SE - 1; y > yescalint_IE; y--) {
			float xf;
			xf = x_prev + declive;
			int x_int = Mathf.RoundToInt(xf);
			perif_mat[x_int,y] = 1;
			x_prev = xf;
		}
	}

	float Declive(float x_e, float y_e, float x_d, float y_d ){
		float larg = Mathf.Abs(x_d - x_e);
		float alt =  y_d - y_e;
		float decl = alt / larg;
		return decl;
	}
}

