using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Camera))]
public class RayMarching : MonoBehaviour
{
	public bool itsTips;
	private VolumeData slicesData;

	[SerializeField]
	[Header("Render in a lower resolution to increase performance.")]
	private int downscale = 2;
	[SerializeField]
	private LayerMask volumeLayer;

	[SerializeField]
	private Shader compositeShader;
	[SerializeField]
	private Shader renderFrontDepthShader;
	[SerializeField]
	private Shader renderBackDepthShader;
	[SerializeField]
	private Shader rayMarchShader;

	[SerializeField][Header("Remove all the darker colors")]
	private bool increaseVisiblity = false;

	[Header("Drag all the textures in here")]
	public float slice_interval = 0.5f;
	public float slices_measure = 25f;
	public float slices_measure_2 = 25f;
	public bool invert;
	[SerializeField]
	private Texture2D[] slices;
	[SerializeField][Range(0, 2)]
	private float opacity = 1;
	[Header("Volume texture size. These must be a power of 2")]
	[SerializeField]
	private int volumeWidth = 256;
	[SerializeField]
	private int volumeHeight = 256;
	[SerializeField]
	private int volumeDepth = 256;
	[Header("Clipping planes percentage")]
	[SerializeField]
	private Vector3 clipDimensions = new Vector3(100, 100, 100);
	//
	[Header("Clipping planes percentage")]
	[SerializeField]
	private Vector3 clipDimensions2 = new Vector3(0, 0, 0);

	private Material _rayMarchMaterial;
	private Material _compositeMaterial;
	private Camera _ppCamera;
	private Texture3D _volumeBuffer;
	
	//private Color[] originalColors;
	private Color[,,] originalpoints;
	private Color[] volumeColors;
	private int[] volume_intens;
	private int[] volume_grad;
	private float[,,] intens_tensor;
	private float[] int_histo;
	private float[] grad_histo;
	public bool Norm;

	//
	public GameObject tela_2d;
	public GameObject color_2d;
	public bool hist_2d;
	private Color[,] original_hist;
	//
	private Color[,] previous_color_mat;
	private int[,] opacity_map;
	private float[,] previous_opac_matrix;
	//
	private float bright = 0.75f;
	private int widgetFace = 6;
	// 1 - back
	// 2- up
	// 3 -down
	// 4 - left
	// 5 - right
	// 6 - forward

	[Header("Game Objects to comunicate to")]
	public Histogram desenho_hist;
	public HistogramGrad desenho_hist_grad;
	public Multiview multiview;
	public bool remote;
	public GameObject volume;
	public GameObject colors;

	public Vector3 getClipDimensions() {
		return this.clipDimensions;
	}

	private void Awake()
	{
		_rayMarchMaterial = new Material(rayMarchShader);
		_compositeMaterial = new Material(compositeShader);
		if (GameObject.FindGameObjectWithTag ("VolumeData") != null) {
			slicesData = GameObject.FindWithTag ("VolumeData").GetComponent<VolumeData> ();
			slices = slicesData.getSlices ();
			Destroy (slicesData);
		}

		if (slices.Length == 0 && itsTips) {
			Application.LoadLevel ("MenuVoxelTips");
		}
	}

	private void Start()
	{
		if (slices.Length != 0) {
			opacity_map = new int[256, 256];
			previous_opac_matrix = new float[256, 256];

			for (int i=1; i<256; i++) {
				for (int j=1; j<256; j++) {
					previous_opac_matrix [i, j] = 1f;
				}
			}

			GenerateVolumeTexture ();
				//Create_int_hist ();
			GenerateGradients ();
				//Create_grad_hist ();
				//if (hist_2d)
					//Hist_2D ();
		}
	}

	private void Update() {
		//if (previous_color_mat == null && hist_2d)
		//	colors.SendMessage ("Update_color", SendMessageOptions.DontRequireReceiver);
	}

	public Texture2D[] getSlices() {
		return slices;
	}

	private void OnDestroy() {
		if(_volumeBuffer != null)
		{
			Destroy(_volumeBuffer);
		}
	}

	[SerializeField]
	private Transform clipPlane;
	[SerializeField]
	private Transform cubeTarget;
	
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		_rayMarchMaterial.SetTexture("_VolumeTex", _volumeBuffer);

		var width = source.width / downscale;
		var height = source.height / downscale;

		if(_ppCamera == null)
		{
			var go = new GameObject("PPCamera");
			_ppCamera = go.AddComponent<Camera>();
			_ppCamera.enabled = false;
		}

		_ppCamera.CopyFrom(GetComponent<Camera>());
		_ppCamera.clearFlags = CameraClearFlags.SolidColor;
		_ppCamera.backgroundColor = Color.white;
		_ppCamera.cullingMask = volumeLayer;

		var frontDepth = RenderTexture.GetTemporary(width, height, 0, RenderTextureFormat.ARGBFloat);
		var backDepth = RenderTexture.GetTemporary(width, height, 0, RenderTextureFormat.ARGBFloat);

		var volumeTarget = RenderTexture.GetTemporary(width, height, 0);

		// need to set this vector because unity bakes object that are non uniformily scaled
		//TODO:FIX
		//Shader.SetGlobalVector("_VolumeScale", cubeTarget.transform.localScale);

		// Render depths
		_ppCamera.targetTexture = frontDepth;
		_ppCamera.RenderWithShader(renderFrontDepthShader, "RenderType");
		_ppCamera.targetTexture = backDepth;
		_ppCamera.RenderWithShader(renderBackDepthShader, "RenderType");

		// Render volume
		_rayMarchMaterial.SetTexture("_FrontTex", frontDepth);
		_rayMarchMaterial.SetTexture("_BackTex", backDepth);

		if(cubeTarget != null && clipPlane != null && clipPlane.gameObject.activeSelf)
		{
			var p = new Plane(
				cubeTarget.InverseTransformDirection(clipPlane.transform.up), 
				cubeTarget.InverseTransformPoint(clipPlane.position));
			_rayMarchMaterial.SetVector("_ClipPlane", new Vector4(p.normal.x, p.normal.y, p.normal.z, p.distance));
		}
		else
		{
			_rayMarchMaterial.SetVector("_ClipPlane", Vector4.zero);
		}

		_rayMarchMaterial.SetFloat("_Opacity", opacity); // Blending strength 
		_rayMarchMaterial.SetVector("_ClipDims", clipDimensions / 100f); // Clip box
		_rayMarchMaterial.SetVector("_ClipDims2", clipDimensions2 / 100f); // Clip box
		_rayMarchMaterial.SetFloat ("_Brt", bright);


		Graphics.Blit(null, volumeTarget, _rayMarchMaterial);

		//Composite
		_compositeMaterial.SetTexture("_BlendTex", volumeTarget);
		Graphics.Blit(source, destination, _compositeMaterial);

		RenderTexture.ReleaseTemporary(volumeTarget);
		RenderTexture.ReleaseTemporary(frontDepth);
		RenderTexture.ReleaseTemporary(backDepth);
	}

	private void GenerateVolumeTexture()
	{
		// sort
		//if (!invert) {
		//	System.Array.Sort (slices, (x, y) => x.name.CompareTo (y.name));
		//} else {
			//System.Array.Sort (slices, (x, y) => y.name.CompareTo (x.name));
		//}

		// use a bunch of memory!
		_volumeBuffer = new Texture3D(volumeWidth, volumeHeight, volumeDepth, TextureFormat.ARGB32, false);
		
		var w = _volumeBuffer.width;
		var h = _volumeBuffer.height;
		var d = _volumeBuffer.depth;

		// skip some slices if we can't fit it all in
		var countOffset = (slices.Length - 1) / (float)d;
		
		volumeColors = new Color[w * h * d];

		// Vector de intensidades
		volume_intens = new int[w * h * d];
		// Tensor de intensidades
		intens_tensor = new float[w, h, d];
		// Tensor de cores
		originalpoints = new Color[w, h, d];
		
		var sliceCount = 0;
		var sliceCountFloat = 0f;
		for(int z = 0; z < d; z++)
		{
			sliceCountFloat += countOffset;
			sliceCount = Mathf.FloorToInt(sliceCountFloat);
			for(int x = 0; x < w; x++)
			{
				for(int y = 0; y < h; y++)
				{
					var idx = x + (y * w) + (z * (w * h));
				
					volumeColors[idx] = slices[sliceCount].GetPixelBilinear(x / (float)w, y / (float)h); 
					if (volumeColors[idx].r ==0 && volumeColors[idx].g ==0 && volumeColors[idx].g ==0){
						volumeColors[idx].a = 0;
					}else{
						volumeColors[idx].a = 1;
					}
					if(increaseVisiblity)
					{
						volumeColors[idx].a *= volumeColors[idx].r;
					}

					Color ints = volumeColors[idx];
					int intensidade = Mathf.RoundToInt(255*(ints.r + ints.g + ints.b) / 3);
					volume_intens[idx] = intensidade;
					intens_tensor[x,y,z] = intensidade;
					originalpoints[x,y,z]=ints;
				}
			}
		}

		// Calcular o gradiente do tensor de intensidades
		///

		//originalColors = volumeColors;
		_volumeBuffer.SetPixels(volumeColors);
		_volumeBuffer.Apply();
		_rayMarchMaterial.SetTexture("_VolumeTex", _volumeBuffer);

		// Definir dimençoes do volume
		float altura_fatias = (slices.Length - 1f) * slice_interval;
		float altura_volume = altura_fatias / slices_measure;
		float largura_volume = slices_measure_2 / slices_measure;
		Vector3 scale = new Vector3 (1f, altura_volume, largura_volume);
		volume.transform.localScale = scale;

		Vector3 dim = new Vector3 (w - 1, h - 1, d - 1);

//		multiview.SendMessage("Set_size", dim , SendMessageOptions.DontRequireReceiver);
//		multiview.SendMessage("Set_scaley", scale, SendMessageOptions.DontRequireReceiver);
//		multiview.SendMessage("Create_thumbnail", originalpoints , SendMessageOptions.DontRequireReceiver);

	}

	void Apply_transfer_function(float[] transfer){

		if(volumeColors != null) {
			for (int i = 0; i < volumeColors.Length; i++) {
				int gray_value = volume_intens[i];
				Color c = volumeColors[i];
				c.a = transfer[gray_value];
				volumeColors[i] = c;
			}

			//print ("tranfer change");
			_volumeBuffer.SetPixels(volumeColors);
			_volumeBuffer.Apply();
			_rayMarchMaterial.SetTexture("_VolumeTex", _volumeBuffer);
		}
	}

	void Apply_transfer_matrix(float[,] opac_matrix){

		if(volumeColors != null) {
			for (int i = 0; i < volumeColors.Length; i++) {
				int ints = volume_intens[i];
				int grad = volume_grad[i];
				Color c = volumeColors[i];
				c.a = opac_matrix[ints,grad];
				volumeColors[i] = c;
			}
		
			_volumeBuffer.SetPixels(volumeColors);
			_volumeBuffer.Apply();
			_rayMarchMaterial.SetTexture("_VolumeTex", _volumeBuffer);
		}
	}

	void Create_int_hist(){

		int_histo = new float[256];
		
		for (int i = 0; i < volume_intens.Length; i++) {
			int intval = volume_intens[i];
			int_histo [intval] += 1f;
		}

		for (int i = 0; i < int_histo.Length; i++) {
			int_histo [i] = Mathf.Pow(int_histo[i],0.15f); 
			if(int_histo[i] < 0f ) int_histo[i] = 0f;
			//print (int_histo[i]);
		}

		int_histo [0] = 0;
		/*
		if (!remote) {
			desenho_hist.SendMessage ("Create_Histo", int_histo, SendMessageOptions.DontRequireReceiver);
		} else {
			nex_1.SendMessage ("Send_hist_intens", int_histo, SendMessageOptions.DontRequireReceiver);
		}
		*/
	}

	void Create_grad_hist(){
		
		grad_histo = new float[256];
		
		for (int i = 0; i < volume_grad.Length; i++) {
			int gradval = volume_grad[i];
			grad_histo [gradval] += 1f;
		}

		for (int i = 0; i < grad_histo.Length; i++) {
			grad_histo [i] = Mathf.Pow(grad_histo[i],0.15f); 
			if(grad_histo[i] < 0f ) grad_histo[i] = 0f;
			//print (grad_histo[i]);
		}

		/*
		if (!remote) {
			desenho_hist_grad.SendMessage ("Create_Histo", grad_histo, SendMessageOptions.DontRequireReceiver);
		}else {
			nex_2.SendMessage ("Send_hist_grad", int_histo, SendMessageOptions.DontRequireReceiver);
		}
		*/
	}


	void Apply_color(Color[] colors){
		for (int i = 0; i < volumeColors.Length; i++) {
			int gray_value = volume_intens[i];
			Color c = volumeColors[i];
			float alpha = c.a;
			Color nc = colors[gray_value];
			nc.a = alpha;
			volumeColors[i] = nc;
		}

		_volumeBuffer.SetPixels(volumeColors);
		_volumeBuffer.Apply();
		_rayMarchMaterial.SetTexture("_VolumeTex", _volumeBuffer);
	}

	void Apply_color_matrix(Color[,] color_mat){

		if (volumeColors != null) {
			for (int i = 0; i < volumeColors.Length; i++) {
				int ints = volume_intens [i];
				int grad = volume_grad [i];

				Color col = color_mat [ints, grad];
				col.a = volumeColors [i].a;
			
				volumeColors [i] = col;
			}

			_volumeBuffer.SetPixels (volumeColors);
			_volumeBuffer.Apply ();
			_rayMarchMaterial.SetTexture ("_VolumeTex", _volumeBuffer);
		}
	}

	void GenerateGradients(){
		
		var w = _volumeBuffer.width;
		var h = _volumeBuffer.height;
		var d = _volumeBuffer.depth;

		volume_grad = new int[w * h * d];
		float[,,] grad_mat = new float[w, h, d];
		//float[,,] grad_scal = new float[w, h, d];
		int[,,] grad_scal_int = new int[w, h, d];

		//float[,,] Sobel_kernel_x = new float[3, 3, 3];
		//float[,,] Sobel_kernel_y = new float[3, 3, 3];
		//float[,,] Sobel_kernel_z = new float[3, 3, 3];

		float minval = 0f;
		float maxval = 0f;
		//float s;
		//float zero_b = 0;
		//float zero = 0;

		for (int z = 1; z < d-1; z++)
		{
			for (int y = 1; y < h-1; y++)
			{
				for (int x = 1; x < w-1 ; x++)
				{
					// Vou obter a intensidade a um dado ponto e vou buscar a intensidade do ponto seguinte (definiçao de tvm
					// a primeira derivada obtem-se pelo modulo do gradiente!
					//s = intens_tensor[x,y,z];
					//if(s==0f)zero_b+=1f;

					// Gradient x
					// Diferenças centradas
					float x1 =  intens_tensor[x+1,y,z] - intens_tensor[x-1,y,z];

					float x2 =  intens_tensor[x+1,y+1,z] - intens_tensor[x-1,y-1,z];
					float x3 =  intens_tensor[x+1,y-1,z] - intens_tensor[x-1,y+1,z];
					float x4 =  intens_tensor[x+1,y,z+1] - intens_tensor[x-1,y,z-1];
					float x5 =  intens_tensor[x+1,y,z-1] - intens_tensor[x-1,y,z+1];

					float x6 =  intens_tensor[x+1,y+1,z+1] - intens_tensor[x-1,y-1,z-1];
					float x7 =  intens_tensor[x+1,y-1,z+1] - intens_tensor[x-1,y+1,z-1];
					float x8 =  intens_tensor[x+1,y+1,z-1] - intens_tensor[x-1,y-1,z+1];
					float x9 =  intens_tensor[x+1,y-1,z-1] - intens_tensor[x-1,y+1,z+1];

					// Gradient
					float grad_x = 6f*x1+3f*(x2+x3+x4+x5)+1f*(x6+x7+x8+x9);

					// Gradient y
					// Diferenças centradas
					float y1 =  intens_tensor[x,y+1,z] - intens_tensor[x,y-1,z];

					float y2 =  intens_tensor[x,y+1,z+1] - intens_tensor[x,y-1,z-1];
					float y3 =  intens_tensor[x,y+1,z-1] - intens_tensor[x,y-1,z+1];
					float y4 =  intens_tensor[x+1,y+1,z] - intens_tensor[x-1,y-1,z];
					float y5 =  intens_tensor[x-1,y+1,z] - intens_tensor[x+1,y-1,z];

					float y6 =  intens_tensor[x+1,y+1,z+1] - intens_tensor[x-1,y-1,z-1];
					float y7 =  intens_tensor[x-1,y+1,z-1] - intens_tensor[x+1,y-1,z+1];
					float y8 =  intens_tensor[x+1,y+1,z-1] - intens_tensor[x-1,y-1,z+1];
					float y9 =  intens_tensor[x-1,y+1,z+1] - intens_tensor[x+1,y-1,z-1];

					// Gradient
					float grad_y = 6f*y1+3f*(y2+y3+y4+y5)+1f*(y6+y7+y8+y9);

					// Gradient y
					// Diferenças centradas
					float z1 =  intens_tensor[x,y,z+1] - intens_tensor[x,y,z-1];

					float z2 =  intens_tensor[x,y+1,z+1] - intens_tensor[x,y-1,z-1];
					float z3 =  intens_tensor[x,y-1,z+1] - intens_tensor[x,y+1,z-1];
					float z4 =  intens_tensor[x+1,y,z+1] - intens_tensor[x-1,y,z-1];
					float z5 =  intens_tensor[x-1,y,z+1] - intens_tensor[x+1,y,z-1];

					float z6 =  intens_tensor[x+1,y+1,z+1] - intens_tensor[x-1,y-1,z-1];
					float z7 =  intens_tensor[x+1,y-1,z+1] - intens_tensor[x-1,y+1,z-1];
					float z8 =  intens_tensor[x-1,y+1,z+1] - intens_tensor[x+1,y-1,z-1];
					float z9 =  intens_tensor[x-1,y-1,z+1] - intens_tensor[x+1,y+1,z-1];

					// Gradient
					float grad_z = 6f*z1+3f*(z2+z3+z4+z5)+1f*(z6+z7+z8+z9);

					if(Norm){
						grad_mat[x,y,z] = Mathf.Sqrt(grad_x*grad_x + grad_y*grad_y + grad_z*grad_z);
					}else{
						grad_mat[x,y,z] = grad_x + grad_y + grad_z;
					}
					//if(grad_mat[x,y,z]==0f)zero+=1f;
					
					if (grad_mat[x,y,z]>maxval){
						maxval = grad_mat[x,y,z];
					}

					if (grad_mat[x,y,z]<minval){
						minval = grad_mat[x,y,z];
					}
				}
			}
		}
		
		for (int z = 0; z <  d; z++)
		{
			for (int x = 0; x < w; x++)
			{
				for (int y = 0; y < h; y++)
				{
					float scalval = grad_mat[x,y,z];
					scalval = ((scalval - minval) / (maxval-minval))*255;
					grad_scal_int[x,y,z] = Mathf.RoundToInt(scalval);

					var idx = x + (y * w) + (z * (w * h));
					volume_grad[idx] = grad_scal_int[x,y,z];
				}
			}
		}
	}

	void TexBox( Vector3[] box_points){
		clipDimensions.x = (0.5f - box_points [1].x)*100;
		clipDimensions.y = (0.5f - box_points [1].z)*100;
		clipDimensions.z = (0.5f + box_points [0].y)*100;
		clipDimensions2.x = (0.5f - box_points [0].x)*100;
		clipDimensions2.y = (0.5f - box_points [0].z)*100;
		clipDimensions2.z = (0.5f + box_points [1].y)*100;
	}
	
	void Hist_2D(){

		float[,] count = new float[256, 256];

		for (int i = 0; i < volume_intens.Length; i++) {
			int grad_val = volume_grad[i];
			int intens_val = volume_intens[i];
			count[intens_val,grad_val] += 1f;
		}

		count [0, 0] = 0f;
		float max_value = 0;
		for (int i=0; i<256; i++) {
			for (int j=0; j<256; j++) {
				count[i,j] = Mathf.Pow(count[i,j],0.12f);
				if(count[i,j]>max_value) max_value = count[i,j];
			}
		}

		for (int i=0; i<256; i++) {
			for (int j=0; j<256; j++) {
				count[i,j] /= max_value;
			}
		}

		Texture2D tela_hist = new Texture2D(256, 256);
		original_hist = new Color[256,256];

		for (int i=0; i<256; i++) {
			for (int j=0; j<256; j++) {
				Color col = Color.Lerp(Color.white, Color.black, count[i,j]);
				original_hist[i,j] = col;
				tela_hist.SetPixel(i,j,col);
			}
		}

		tela_hist.Apply ();
		tela_2d.GetComponent<Renderer>().material.mainTexture = tela_hist;
	}

	void Hist_2D_exterior(float[,] count){

		Texture2D tela_hist = new Texture2D(256, 256);
		
		for (int i=0; i<256; i++) {
			for (int j=0; j<256; j++) {
				Color col = Color.Lerp(Color.white, Color.black, count[i,j]);
				tela_hist.SetPixel(i,j,col);
			}
		}
		
		tela_hist.Apply ();
		tela_2d.GetComponent<Renderer>().material.mainTexture = tela_hist;
	}

	void Update_Hist_2D(Color[,] color_mat){

		if (opacity_map == null) {
			opacity_map = new int[256, 256];
		}

		Texture2D new_tela = new Texture2D(256, 256);
		previous_color_mat = color_mat;
		
		for (int i=0; i<256; i++) {
			for (int j=0; j<256; j++) {
				//Color col = Color.Lerp(color_mat[i,j],original_hist[i,j],0.75f);
				Color col = color_mat[i,j];
				if(opacity_map[i,j]==1){
					col.a=0.7f;
				}else{
					col.a=0f;
				}
				new_tela.SetPixel(i,j,col);
			}
		}

		new_tela.Apply ();
		color_2d.GetComponent<Renderer>().material.mainTexture = new_tela;
	}

	void Paint_map(int[,] map){
		
		Texture2D new_tela = new Texture2D(256, 256);
		
		for (int i=0; i<256; i++) {
			for (int j=0; j<256; j++) {
				if(map[i,j]==1){
					Color col = Color.black;
					new_tela.SetPixel(i,j,col);
				}else{
					Color col = Color.white;
					new_tela.SetPixel(i,j,col);
				}
			}
		}
		
		new_tela.Apply ();
		tela_2d.GetComponent<Renderer>().material.mainTexture = new_tela;
	}

	void Apply_opac_map(int[,] map){
		
		for (int i=0; i<256; i++) {
			for (int j=0; j<256; j++) {
				if(map[i,j]==1){
					opacity_map[i,j] = 1;
				}
				if(map[i,j]==2){
					opacity_map[i,j] = 0;
				}
			}
		}

		Update_Hist_2D (previous_color_mat);

		for (int i = 0; i < volumeColors.Length; i++) {
			int ints = volume_intens[i];
			int grad = volume_grad[i];
			Color c = volumeColors[i];
			if(opacity_map[ints,grad]==1){
				c.a = previous_opac_matrix[ints,grad];
			}else{
				c.a=0f;
			}
			volumeColors[i] = c;
		}

		_volumeBuffer.SetPixels(volumeColors);
		_volumeBuffer.Apply();
		_rayMarchMaterial.SetTexture("_VolumeTex", _volumeBuffer);
	}

	void Apply_transfer_matrix_2d(float[,] opac_matrix){

		previous_opac_matrix = opac_matrix;

		for (int i = 0; i < volumeColors.Length; i++) {
			int ints = volume_intens[i];
			int grad = volume_grad[i];
			Color c = volumeColors[i];
			if(opacity_map[ints,grad]==1){
				c.a = opac_matrix[ints,grad];
			}else{
				c.a=0f;
			}
			volumeColors[i] = c;
		}

		_volumeBuffer.SetPixels(volumeColors);
		_volumeBuffer.Apply();
		_rayMarchMaterial.SetTexture("_VolumeTex", _volumeBuffer);
	}

	void Set_brightness(float brt){
		bright = brt;
	}

	void Apply_transfer_step(int step){
		
		for (int i = 0; i < volumeColors.Length; i++) {
			int gray_value = volume_intens[i];
			Color c = volumeColors[i];
			if(gray_value<=step){
				c.a = 0f;
			}else{
				c.a = 1f;
			}
			volumeColors[i] = c;
		}

		_volumeBuffer.SetPixels(volumeColors);
		_volumeBuffer.Apply();
		_rayMarchMaterial.SetTexture("_VolumeTex", _volumeBuffer);
	}

	public int getWidgetFace() {
		return widgetFace;
	}

	public void setWidgetFace(int widgetFace) {
		this.widgetFace = widgetFace;
	}

	public void setSlicing (float[] data) {
		widgetFace = (int)data[1];
		switch(widgetFace) {
			case 1: //costas
				//data[0] *= -1; Descomentar para cortes segundo todos os eixos
				if((clipDimensions2.y + data[0]) >= 0 && (clipDimensions2.y + data[0]) <= 100)
					clipDimensions2.y += data[0];
				break;
			case 2:
				//data[0] *= -1; Descomentar para cortes segundo todos os eixos
				if((clipDimensions.z - data[0]) >= 0 && (clipDimensions.z - data[0]) <= 100)
					clipDimensions.z -= data[0];
				break;
			case 3:
				if((clipDimensions2.z + data[0]) >= 0 && (clipDimensions2.z + data[0]) <= 100)
					clipDimensions2.z += data[0];
				break;
			case 4:
				if((clipDimensions.x - data[0]) >= 0 && (clipDimensions.x - data[0]) <= 100)
					clipDimensions.x -= data[0];
				break;
			case 5:
				//data[0] *= -1; Descomentar para cortes segundo todos os eixos
				if((clipDimensions2.x + data[0]) >= 0 && (clipDimensions2.x + data[0]) <= 100)
					clipDimensions2.x += data[0];
				break;
			case 6:
				if((clipDimensions.y - data[0]) >= 0 && (clipDimensions.y - data[0]) <= 100)
					clipDimensions.y -= data[0];
				break;
		}
	}
}
