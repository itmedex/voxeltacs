﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Function_draw : MonoBehaviour {


	public LayerMask touchInputMask;
	
	//private List<GameObject> touchList = new List<GameObject>();
	private GameObject[] touchesOld;
	
	public GameObject cursor;
	
	public int[] func;
	
	// Toggle Lock
	public bool unlocked = true;
	
	public float xfloat;
	public float yfloat;
	
	private RaycastHit hit;

	//public Manipulateobject screen;
	public GameObject sliding_boxes;
	public GameObject ramp;

	// Screen var
	private Vector3 initialcoor;
	private Vector3 finalcoor;
	private float screen_height;
	private float screen_lenght;
	public float top;
	public float bottom;
	public float right;
	public float left;

	void Start(){
		screenBounds ();
	}

	void Update () {

		if (sliding_boxes.activeSelf || ramp.activeSelf) {
			unlocked = false;
		}

		if (unlocked) {
			
			if (TuioInput.touchCount > 0) {

				foreach (Touch touch in TuioInput.touches) {
					
					xfloat = touch.position.x;
					yfloat = touch.position.y;

					if (touch.phase == TouchPhase.Began && xfloat < right && xfloat > left && yfloat < top && yfloat > bottom) {
						//recipient.SendMessage ("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
						cursor.SendMessage ("makeLine", SendMessageOptions.DontRequireReceiver);
					}
					if (touch.phase == TouchPhase.Ended) {
						//recipient.SendMessage ("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);
						cursor.SendMessage ("FinishLine", SendMessageOptions.DontRequireReceiver);
					}
					if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) {
						//recipient.SendMessage ("OnTouchStay", hit.point, SendMessageOptions.DontRequireReceiver);
						cursor.SendMessage ("UpdateLine", SendMessageOptions.DontRequireReceiver);
					}
					//if (touch.phase == TouchPhase.Canceled) {
						//recipient.SendMessage ("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
					//}
				}
			}
		}
	}

	void screenBounds () {
		Camera cam = GetComponent<Camera>();
		
		Vector3 screenPosfi = cam.WorldToScreenPoint(transform.position);
		
		Vector3 centralpixel = screenPosfi;
		
		screen_height = cam.pixelHeight;
		screen_lenght = cam.pixelWidth;
		
		top = centralpixel.y + screen_height / 2;
		bottom = centralpixel.y - screen_height / 2;
		left = centralpixel.x - screen_lenght / 2;
		right = centralpixel.x + screen_lenght / 2;
	}
	
	public void Toggle_Lock (){
		unlocked = !unlocked;
	}
}