﻿using UnityEngine;
using System.Collections;

public class CustomFunction : MonoBehaviour {

	// Classe onde vai buscar a funçao incompleta
	public Cursor fonte;
	public Cursor fonte_grad;
	public Cursor fonte_2;
	public Cursor fonte_grad_2;

	// Funçao incompleta
	private float[] inc_trans;

	// Funçao completa de intensidades
	public float[] funcao_transferencia;

	// Funçao completa de gradientes
	public float[] funçao_trans_grad;

	// Funçoes secundarias para hist2D
	public float[] funcao_transferencia_2;
	public float[] funçao_trans_grad_2;

	// Intervalo a analisar
	public int nr = 20;

	// Classe que aplica a funçao
	public RayMarching volume;

	// thresholds
	public float max;
	public float min;

	// Map2D
	public bool map2d;

	//remote
	public bool remote_send;

	// Tablet Reference
	GameObject tab;

	void Start() {

		tab = GameObject.FindGameObjectWithTag ("Nexus");

		funcao_transferencia = new float[256];
		funçao_trans_grad = new float[256];
		//funcao_transferencia_2 = new float[256];
		//funçao_trans_grad_2 = new float[256];

		for (int i=1; i<funcao_transferencia.Length; i++) {
			funcao_transferencia[i]=1f;
		}

		for (int i=0; i<funçao_trans_grad.Length; i++) {
			funçao_trans_grad[i]=1f;
		}
		/*
		for (int i=1; i<funcao_transferencia_2.Length; i++) {
			funcao_transferencia_2[i]=1f;
		}
		
		for (int i=0; i<funçao_trans_grad_2.Length; i++) {
			funçao_trans_grad_2[i]=1f;
		}
		*/
	
	}

	void DefineMatrixInt(float[] tran){
		funcao_transferencia = tran;
		Define_matrix ();
	}

	void DefineMatrixGrad(float[] tran){
		funçao_trans_grad = tran;
		Define_matrix ();
	}

	void Define_matrix(){

		float[,] opac_matrix = new float[256, 256];

		for (int i=0; i < 256; i++) {
			for (int j=0; j<256; j++){
				opac_matrix[i,j] = funcao_transferencia[i]*funçao_trans_grad[j];
			}
		}
		volume.SendMessage ("Apply_transfer_matrix", opac_matrix, SendMessageOptions.DontRequireReceiver);
	}
	
	void UpdateFunction() {

		inc_trans = fonte.transf;

		int low = 255;
		int high = 0;
		
		for (int i=0; i<inc_trans.Length; i++) {
			
			if (inc_trans[i] >= 0f && i <= low) {
				low = i;
			}
			
			if (inc_trans[i] >= 0f && i > high) high = i;
		}

		//Aproximate ();
		for (int x=1; x < inc_trans.Length - 1; x++) {
			
			if(x > low && x <= high){
				
				if (inc_trans[x]<0f){
					
					// Percorrer os dados do vector - stop se encontrar diferente de zero ou chegar ao fim
					int idx = x + 1;
					while(idx < high && inc_trans[idx]<=0f ){
						idx++;
					}
					
					// Se encontrou um valor diferente de zero
					inc_trans[x] = inc_trans[x-1] + Declive (x-1, idx);
					
				}
			}
		}
		
		for (int x=0; x < inc_trans.Length - 1; x++) {
			if(inc_trans[x] < min){
				inc_trans[x] = 0f;
			}
			
			if(inc_trans[x] > max){
				inc_trans[x]=1f;
			}
		}

		for (int it = low; it <= high; it++) {

			funcao_transferencia[it] = inc_trans[it];
		}

		//print ("Update complete");
		//volume.SendMessage("Apply_transfer_function", funcao_transferencia ,SendMessageOptions.DontRequireReceiver);
		/*if (!map2d) {
			if (remote_send) {
				GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
				nex.SendMessage ("Send_function_opac_array", funcao_transferencia, SendMessageOptions.DontRequireReceiver);
			} else {
				Define_matrix ();
			}
		} else {
			//Map_2D ();
			Define_matrix ();
		}*/

		if (remote_send) {
			GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
			nex.SendMessage ("Send_function_opac_array", funcao_transferencia, SendMessageOptions.DontRequireReceiver);
		} else {
			Define_matrix ();
		}
	}

	void UpdateFunction_Grad() {

		inc_trans = fonte_grad.transf;
		
		int low = 255;
		int high = 0;
		
		for (int i=0; i<inc_trans.Length; i++) {
			
			if (inc_trans[i] >= 0f && i <= low) {
				low = i;
			}
			
			if (inc_trans[i] >= 0f && i > high) high = i;
		}
		
		//Aproximate ();
		for (int x=1; x < inc_trans.Length - 1; x++) {
			
			if(x > low && x <= high){
				
				if (inc_trans[x]<0f){
					
					// Percorrer os dados do vector - stop se encontrar diferente de zero ou chegar ao fim
					int idx = x + 1;
					while(idx < high && inc_trans[idx]<=0f ){
						idx++;
					}
					
					// Se encontrou um valor diferente de zero
					inc_trans[x] = inc_trans[x-1] + Declive (x-1, idx);
					
				}
			}
		}
		
		for (int x=0; x < inc_trans.Length - 1; x++) {
			if(inc_trans[x] < min){
				inc_trans[x] = 0f;
			}
			
			if(inc_trans[x] > max){
				inc_trans[x]=1f;
			}
		}
		
		for (int it = low; it <= high; it++) {
			
			funçao_trans_grad[it] = inc_trans[it];
		}
		
		//print ("Update complete");
		//volume.SendMessage("Apply_transfer_function", funcao_transferencia ,SendMessageOptions.DontRequireReceiver);
		/*if (!map2d) {
			if (remote_send) {
				GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
				nex.SendMessage ("Send_function_opac_array", funçao_trans_grad, SendMessageOptions.DontRequireReceiver);
			} else {
				Define_matrix ();
			}
		} else {
			//Map_2D ();
			Define_matrix ();
		}*/

		if (remote_send) {
			GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
			nex.SendMessage ("Send_function_grad_array", funçao_trans_grad, SendMessageOptions.DontRequireReceiver);
		} else {
			Define_matrix ();
		}
	}

	void UpdateFunction_2() {
		
		inc_trans = fonte_2.transf;
		
		int low = 255;
		int high = 0;
		
		for (int i=0; i<inc_trans.Length; i++) {
			
			if (inc_trans[i] >= 0f && i <= low) {
				low = i;
			}
			
			if (inc_trans[i] >= 0f && i > high) high = i;
		}
		
		//Aproximate ();
		for (int x=1; x < inc_trans.Length - 1; x++) {
			
			if(x > low && x <= high){
				
				if (inc_trans[x]<0f){
					
					// Percorrer os dados do vector - stop se encontrar diferente de zero ou chegar ao fim
					int idx = x + 1;
					while(idx < high && inc_trans[idx]<=0f ){
						idx++;
					}
					
					// Se encontrou um valor diferente de zero
					inc_trans[x] = inc_trans[x-1] + Declive (x-1, idx);
					
				}
			}
		}
		
		for (int x=0; x < inc_trans.Length - 1; x++) {
			if(inc_trans[x] < min){
				inc_trans[x] = 0f;
			}
			
			if(inc_trans[x] > max){
				inc_trans[x]=1f;
			}
		}
		
		for (int it = low; it <= high; it++) {
			
			funcao_transferencia_2[it] = inc_trans[it];
		}
		
		//print ("Update complete");
		//volume.SendMessage("Apply_transfer_function", funcao_transferencia ,SendMessageOptions.DontRequireReceiver);
		//Define_matrix ();

		if (!map2d) {
			Define_matrix ();
		} else {
			//Map_2D ();
			Define_matrix ();
		}
	}

	void UpdateFunction_Grad_2() {
		
		print ("update gradient");
		
		inc_trans = fonte_grad_2.transf;
		
		int low = 255;
		int high = 0;
		
		for (int i=0; i<inc_trans.Length; i++) {
			
			if (inc_trans[i] >= 0f && i <= low) {
				low = i;
			}
			
			if (inc_trans[i] >= 0f && i > high) high = i;
		}
		
		//Aproximate ();
		for (int x=1; x < inc_trans.Length - 1; x++) {
			
			if(x > low && x <= high){
				
				if (inc_trans[x]<0f){
					
					// Percorrer os dados do vector - stop se encontrar diferente de zero ou chegar ao fim
					int idx = x + 1;
					while(idx < high && inc_trans[idx]<=0f ){
						idx++;
					}
					
					// Se encontrou um valor diferente de zero
					inc_trans[x] = inc_trans[x-1] + Declive (x-1, idx);
					
				}
			}
		}
		
		for (int x=0; x < inc_trans.Length - 1; x++) {
			if(inc_trans[x] < min){
				inc_trans[x] = 0f;
			}
			
			if(inc_trans[x] > max){
				inc_trans[x]=1f;
			}
		}
		
		for (int it = low; it <= high; it++) {
			
			funçao_trans_grad_2[it] = inc_trans[it];
		}
		
		//print ("Update complete");
		//volume.SendMessage("Apply_transfer_function", funcao_transferencia ,SendMessageOptions.DontRequireReceiver);
		//Define_matrix ();

		if (!map2d) {
			Define_matrix ();
		} else {
		//	Map_2D ();
			Define_matrix ();
		}
	}

	float Declive(int ponto_esq, int ponto_dir){
		int larg = ponto_dir - ponto_esq;
		float alt = inc_trans [ponto_dir] - inc_trans [ponto_esq];
		float decl = alt / larg;
		return decl;
	}

	void Aproximate(){

		for (int i=0; i<inc_trans.Length; i++) {
			if(inc_trans[i] >= 0f){ 
				float val1 = inc_trans[i] * 10;
				int val2 = Mathf.RoundToInt(val1);
				float val3 = val2/10f;
				inc_trans[i]=val3;
			}
		}
	}

	void Map_2D (){

		float[,] map = new float[256, 256];

		for (int i=0; i < 256; i++) {
			for (int j=0; j<256; j++){

				float c1 = (255f - j)/255f;
				float c2 = 1f - c1;
				float c3 = (255f - i)/255f;
				float c4 = 1f - c3;

				map[i,j] = c1*funcao_transferencia[i] + c2*funcao_transferencia_2[i] + c3*funçao_trans_grad[j] + c4*funçao_trans_grad_2[j];
				map[i,j] = map[i,j]/2f;
			}
		}
		volume.SendMessage("Apply_transfer_matrix_2d", map ,SendMessageOptions.DontRequireReceiver);
	}

}
