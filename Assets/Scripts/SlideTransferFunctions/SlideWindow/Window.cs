﻿using UnityEngine;
using System.Collections;

public class Window : MonoBehaviour {

	Camera touchcamera;
	public Camera usecamera;
	private bool _mouseState;
	public GameObject Target;
	public Vector3 screenSpace;
	public Vector3 offset;

	// Origem dos limites
	public Cursor cursor;
	
	// variavel touch
	private Touch touch;

	// Objectos da janela
	public GameObject ref_L;
	public GameObject ref_R;
	
	// Pontos de medida
	float left_point;
	float right_point;
	float height;
	float floor;

	//
	public TouchMovement screen;

	// Use this for initialization
	void Start ()
	{
		touchcamera = usecamera;
	}
	
	// Update is called once per frame
	void Update ()
	{
		left_point = ref_L.transform.position.x;
		right_point = ref_R.transform.position.x;
		height = ref_L.transform.position.z;
		floor = cursor.initialcoor.z;

		Check_position();

		Check_scale();

		// Debug.Log(_mouseState);
		if (TuioInput.touchCount > 0) {
			touch = TuioInput.touches [0];
		}
		
		if (touch.phase == TouchPhase.Began && screen.unlocked == true) {
			RaycastHit hitInfo;
			if (Target == GetClickedObject (out hitInfo)) {
				_mouseState = true;
				screenSpace = touchcamera.WorldToScreenPoint (Target.transform.position);
				offset = Target.transform.position - touchcamera.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, screenSpace.z));
			}
		}
		if (touch.phase == TouchPhase.Ended) {
			_mouseState = false;
		}
		if (_mouseState) {
			//keep track of the mouse position
			var curScreenSpace = new Vector3 (touch.position.x, touch.position.y, screenSpace.z);
			
			//convert the screen mouse position to world point and adjust with offset
			var curPosition = touchcamera.ScreenToWorldPoint (curScreenSpace) + offset;
			
			//update the position of the object in the world
			Target.transform.position = curPosition;

			CheckPos ();
		}
	}
	
	
	GameObject GetClickedObject (out RaycastHit hit)
	{
		GameObject target = null;
		Ray ray = touchcamera.ScreenPointToRay (touch.position);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}
	
	void CheckPos(){

		float largura = right_point - left_point;
		Vector3 newpos = transform.position;
		
		if (transform.position.x > cursor.finalcoor.x - largura/2) {
			newpos.x = cursor.finalcoor.x - largura/2;
		}
		if (transform.position.x < cursor.initialcoor.x + largura/2) {
			newpos.x = cursor.initialcoor.x + largura/2;
		}
		if (transform.position.z != (height + floor) / 2) {
			newpos.z = (height + floor) / 2;
		}
	
		if (newpos != transform.position) {
			transform.position = newpos;
		}
		
	}
	
	void Check_position(){
		
		float pos_x = (left_point + right_point) / 2;
		float pos_z = (height + floor) / 2;
		
		Vector3 new_pos = new Vector3 (pos_x, 3f, pos_z);
		
		transform.position = new_pos;
	}
	
	void Check_scale(){
		
		float altura = height - floor;
		float largura = right_point - left_point;
		
		Vector3 new_scale = new Vector3 (largura, 0.04f, altura);
		
		transform.localScale = new_scale;
		
	}
}
