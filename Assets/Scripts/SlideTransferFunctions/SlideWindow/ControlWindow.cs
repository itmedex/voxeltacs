﻿using UnityEngine;
using System.Collections;

public class ControlWindow : MonoBehaviour {

	// Origem das medidas
	public Cursor cursor;

	// Objectos da janela
	public GameObject window;
	public GameObject ref_L;
	public GameObject ref_R;

	// Pontos de medida
	float left_point;
	float right_point;
	float height;
	float floor;

	// pontos de referencia
	float last_R;
	float last_L;

	// Vector da funçao de trenferencia
	public float[] transfer_func;

	public CustomFunction customs;

	public bool intens;

	private GameObject tabs;
	
	void Start () {
		
		tabs = GameObject.FindGameObjectWithTag ("Nexus");

		transfer_func = new float[256];
		Define_transfer ();
		//grapher.SendMessage ("Window_func", SendMessageOptions.DontRequireReceiver);
	}

	void Update() {
	
		left_point = ref_L.transform.position.x;
		right_point = ref_R.transform.position.x;

		if (last_L != left_point || last_R != right_point) {
			height = ref_L.transform.position.z;
			floor = cursor.initialcoor.z;
			Define_transfer ();
			//grapher.SendMessage ("Window_func", SendMessageOptions.DontRequireReceiver);
			last_L = left_point;
			last_R = right_point;
		} 

	}

	void Define_transfer(){

		float screen_height = cursor.finalcoor.z - cursor.initialcoor.z;
		float window_height = height - floor;

		float opacidade = window_height / screen_height;

		float screen_lenght = cursor.finalcoor.x - cursor.initialcoor.x;
		float L_length = left_point - cursor.initialcoor.x;
		float R_length = right_point - cursor.initialcoor.x;

		float left_pos = L_length / screen_lenght;
		float right_pos = R_length / screen_lenght;

		float l_intensity = left_pos * 255;
		float r_intensity = right_pos * 255;

		int min_value = Mathf.RoundToInt (l_intensity);
		int max_value = Mathf.RoundToInt (r_intensity);

		for (int i=0; i<transfer_func.Length; i++) {
			if(i >= min_value && i <= max_value){
				transfer_func[i] = opacidade;
			}else{
				transfer_func[i]=0;
			}
		}

		if (intens) {
			tabs.SendMessage ("Send_function_opac_array", transfer_func, SendMessageOptions.DontRequireReceiver);
		} else {
			tabs.SendMessage ("Send_function_grad_array", transfer_func, SendMessageOptions.DontRequireReceiver);
		}
	}
}
