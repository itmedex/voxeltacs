﻿using UnityEngine;
using System.Collections;

public class Janela_ref_l : MonoBehaviour {

	Camera touchcamera;
	public Camera usecamera;
	private bool _mouseState;
	public GameObject Target;
	public Vector3 screenSpace;
	public Vector3 offset;
	
	// Origem dos limites
	public Cursor cursor;
	
	// variavel touch
	private Touch touch;

	// Objecto 
	public GameObject Right;
	public GameObject window;

	//
	public TouchMovement screen;

	// Use this for initialization
	void Start ()
	{
		touchcamera = usecamera;
	}
	
	// Update is called once per frame
	void Update ()
	{
		// Unico codigo original
		CheckPos ();

		// Debug.Log(_mouseState);
		if (TuioInput.touchCount > 0) {
			touch = TuioInput.touches [0];
		}
		
		if (touch.phase == TouchPhase.Began && screen.unlocked == true) {
			RaycastHit hitInfo;
			if (Target == GetClickedObject (out hitInfo)) {
				_mouseState = true;
				screenSpace = touchcamera.WorldToScreenPoint (Target.transform.position);
				offset = Target.transform.position - touchcamera.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, screenSpace.z));
			}
		}
		if (touch.phase == TouchPhase.Ended) {
			_mouseState = false;
		}
		if (_mouseState) {
			//keep track of the mouse position
			var curScreenSpace = new Vector3 (touch.position.x, touch.position.y, screenSpace.z);
			
			//convert the screen mouse position to world point and adjust with offset
			var curPosition = touchcamera.ScreenToWorldPoint (curScreenSpace) + offset;
			
			//update the position of the object in the world
			Target.transform.position = curPosition;

			CheckPos_2();
		}
	}
	
	
	GameObject GetClickedObject (out RaycastHit hit)
	{
		GameObject target = null;
		Ray ray = touchcamera.ScreenPointToRay (touch.position);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}
	
	void CheckPos(){
		
		Vector3 newpos = transform.position;

		if (transform.position.x > Right.transform.position.x) {
			newpos.x = Right.transform.position.x;
		}
		if (transform.position.z != Right.transform.position.z) {
			newpos.z = Right.transform.position.z;
		}
		if (transform.position.x != window.transform.position.x - window.transform.localScale.x / 2) {
			newpos.x = window.transform.position.x - window.transform.localScale.x / 2;
		}
		if (newpos != transform.position) {
			transform.position = newpos;
		}
	}

	void CheckPos_2(){
		
		Vector3 newpos = transform.position;

		if (transform.position.x < cursor.initialcoor.x) {
			newpos.x = cursor.initialcoor.x;
		}
		if (transform.position.z < cursor.initialcoor.z) {
			newpos.z = cursor.initialcoor.z;
		}
		if (transform.position.z > cursor.finalcoor.z) {
			newpos.z = cursor.finalcoor.z;
		}
		if (newpos != transform.position) {
			transform.position = newpos;
		}
	}
}
