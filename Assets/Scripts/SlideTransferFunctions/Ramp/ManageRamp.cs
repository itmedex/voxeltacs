﻿using UnityEngine;
using System.Collections;

public class ManageRamp : MonoBehaviour {
	
	// Origem das medidas da camara
	public Cursor cursor;
	
	// Camera variables
	public Camera touchcamera;
	// Medidas da camara para normalizaçao
	public float screen_lenght;
	public float screen_height;
	
	// Normalization measurementes
	Vector3 centralpixel;
	
	// Mediçoes de ecra
	public Vector3 initialcoor;
	public Vector3 finalcoor;
	private float plane_lenght;
	private float plane_height;
	
	// Mediçoes dos rectangulos
	private float rect_lenght;
	private float rect_height;
	
	// Rectangulos a manipular
	public GameObject[] refs;

	// Posiçoes em z de cada rectangulo
	private float[] worldz;
	private float[] scaledval;
	public float z_min;
	public float z_max;
	
	// Funçao de tranferencia
	public float[] trans_func;

	public CustomFunction customs;

	public bool intens;

	private GameObject tabs;
	
	void Start () {
		
		tabs = GameObject.FindGameObjectWithTag ("Nexus");
		
		// define as medidas da tela
		screenBounds ();
		
		// inicializa os vectores
		trans_func = new float[256];
		worldz= new float[11];
		scaledval = new float[11];

		z_max = finalcoor.z;
		z_min = initialcoor.z;
		
		rect_lenght = plane_lenght/ 10f;
		rect_height = plane_height;

		// Define o tamanho e posiçoes dos rectangulos
		Set_measures ();
	
		// Define um valor entre 0 e 1 tendo em conta a posiçao dos rectangulos
		Scale ();
		
		// Define a funçaod e transferencia para os vectores
		//Transfer_func ();
		Adapt_1D ();

		StartCoroutine(UpdateFunction());
	}
	
	IEnumerator UpdateFunction()
	{
		while(true)
		{
			ManageRamps();
			yield return new WaitForSeconds(2);
		}
	}

	void ManageRamps () {
		
		float[] newpos = new float[11];
		bool changed = false;
		
		for (int i=0; i < newpos.Length; i++) {
			newpos[i] = refs[i].transform.position.z;
			
			if(worldz[i]!= newpos[i]){
				changed = true;
			}
		}
		
		if (changed) {
			worldz = newpos;
			Scale();
			Transfer_func();
			//grapher.SendMessage ("Apply_function", trans_func , SendMessageOptions.DontRequireReceiver);
		}
	}
	
	void Set_measures(){

		for (int i=0; i < refs.Length; i++) {
			
			Vector3 worldpos = new Vector3 (initialcoor.x + rect_lenght*i, 3f, initialcoor.z);
			refs[i].transform.position = worldpos;
			
			worldz[i]= worldpos.z;
		}
	}
	
	void screenBounds () {
		
		Vector3 screenPosfi = touchcamera.WorldToScreenPoint(touchcamera.transform.position);
		
		centralpixel = screenPosfi;
		
		screen_height = touchcamera.pixelHeight;
		screen_lenght = touchcamera.pixelWidth;
		
		initialcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x - screen_lenght / 2, centralpixel.y - screen_height / 2, 0f));
		finalcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x + screen_lenght / 2, centralpixel.y + screen_height / 2, 0f));
		
		plane_height = finalcoor.z - initialcoor.z;
		plane_lenght = finalcoor.x - initialcoor.x;
		
	}
	
	void Scale(){
		
		for (int i=0; i< worldz.Length; i++) {
			
			float cval = worldz[i] - z_min;
			
			scaledval[i] = cval / rect_height;
		}
		
	}
	
	void Transfer_func(){
		
		float div = 255f / 10f;
		
		for (int i = 0; i< scaledval.Length -1; i++) {
			
			int one = Mathf.RoundToInt(i*div);
			int two = Mathf.RoundToInt((i+1)*div);

			for(int j= one; j<= two; j++){
				float t = (j-one)/div;
				trans_func[j] = Mathf.Lerp(scaledval[i], scaledval[i+1], t);
			}
		}

		if (intens) {
			tabs.SendMessage ("Send_function_opac_array", trans_func, SendMessageOptions.DontRequireReceiver);
		} else {
			tabs.SendMessage ("Send_function_grad_array", trans_func, SendMessageOptions.DontRequireReceiver);
		}
	}
	
	void Adapt_1D (){

		//declaracao do vector que importei do cursor
		float[] transf_vector;
		if (intens) {
			transf_vector = customs.funcao_transferencia;
		} else {
			transf_vector = customs.funçao_trans_grad;
		}

		Vector3 pos_1 = refs [0].transform.position;
		pos_1.z = initialcoor.z + (transf_vector[0] * rect_height);
		refs [0].transform.position = pos_1;

		Vector3 pos_2 = refs [10].transform.position;
		pos_2.z = initialcoor.z + (transf_vector[255] * rect_height);
		refs [10].transform.position = pos_2;

		//Intervalos de intensidades
		float div = 255f / 10f;
		
		for (int i = 1; i< scaledval.Length - 1; i++) {
			
			int one = Mathf.RoundToInt((i-1)*div);
			int two = Mathf.RoundToInt((i+1)*div);
			
			float total = 0;
			float nr = 0;
			for(int j= one; j<= two; j++){
				if(transf_vector[j]!=0){
					total = total + transf_vector[j];
					nr = nr + 1f;
				}
			}
			
			float mean = total / nr;
			if (mean <= 1f && mean >= 0f) {
			} else {
				mean=0f;
			}
			Vector3 pos = refs [i].transform.position;
			pos.z = initialcoor.z + (mean * rect_height);
			refs [i].transform.position = pos;
		}

	}

}
