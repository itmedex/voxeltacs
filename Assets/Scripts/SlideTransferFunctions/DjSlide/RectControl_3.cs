﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RectControl_3 : MonoBehaviour {

	Camera touchcamera;
	public Camera usecamera;
	
	// Posiçao de refrencia
	private bool set = true;
	private Vector3 worldpos;
	
	// variavel touch
	private Touch touch;
	
	public TouchMovement screen;
	public ManageRects manage;
	private float zmax;
	private float zmin;

	// tempo para os switch
	private float safetime;

	// Intens
	public bool intens;
	
	void Start () {
		touchcamera = usecamera;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (set) {
			worldpos = transform.position;
			set = false;
			zmax = manage.z_max;
			zmin = manage.z_min;
		}

		if (TuioInput.touchCount > 0 && screen.unlocked == true) {

			foreach(Touch touch in TuioInput.touches){

				Vector3 newpos = touchcamera.ScreenToWorldPoint(new Vector3 (touch.position.x, touch.position.y, 0f));

				if( newpos.x > transform.position.x - manage.rect_lenght/2 && newpos.x < transform.position.x + manage.rect_lenght/2 && newpos.z < manage.finalcoor.z && newpos.z > manage.initialcoor.z){

					Vector3 oldpos = transform.position;
					oldpos.z = newpos.z - manage.rect_height/2;
					transform.position = oldpos;
				}

				if(intens && newpos.x > transform.position.x - manage.rect_lenght/2 && newpos.x < transform.position.x + manage.rect_lenght/2 && newpos.z < manage.initialcoor.z && safetime < Time.time){

					if(Mathf.Abs(transform.position.z - zmin) < 0.5f){
						Vector3 oldpos = transform.position;
						oldpos.z = zmax;
						transform.position = oldpos;
					}else{
						Vector3 oldpos = transform.position;
						oldpos.z = zmin;
						transform.position = oldpos;
					}
					safetime = Time.time + 0.35f;
				}
			}
		}

		CheckPos ();
	}
	
	void CheckPos(){
		
		Vector3 newpos = transform.position;
		
		if (transform.position.x != worldpos.x) {
			newpos.x = worldpos.x;
		}

		if (transform.position.y != worldpos.y) {
			newpos.y = worldpos.y;
		}

		if (transform.position.z > zmax) {
			newpos.z = zmax;
		}
		
		if (transform.position.z < zmin) {
			newpos.z = zmin;
		}
		
		transform.position = newpos;
	}
}
