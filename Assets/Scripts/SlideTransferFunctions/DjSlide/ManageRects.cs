﻿using UnityEngine;
using System.Collections;

public class ManageRects : MonoBehaviour {

	// Camera variables
	public Camera touchcamera;
	// Medidas da camara para normalizaçao
	public float screen_lenght;
	public float screen_height;

	// Normalization measurementes
	Vector3 centralpixel;
	
	// Mediçoes de ecra
	public Vector3 initialcoor;
	public Vector3 finalcoor;
	private float plane_lenght;
	private float plane_height;

	// Mediçoes dos rectangulos
	public float rect_lenght;
	public float rect_height;
	
	// Rectangulos a manipular
	public GameObject[] rects;

	// Posiçoes em z de cada rectangulo
	private float[] worldz;
	private float[] scaledval;
	public float z_min;
	public float z_max;

	public CustomFunction customs;

	// Funçao de tranferencia
	public float[] trans_func;

	// Intens
	public bool intens;

	private GameObject tabs;
	
	void Start () {

		tabs = GameObject.FindGameObjectWithTag ("Nexus");

		// define as medidas da tela
		screenBounds ();

		// medidas dos rectangulos
		rect_lenght = plane_lenght / 10f;
		rect_height = plane_height;

		// inicializa os vectores
		trans_func = new float[256];
		worldz= new float[10];
		scaledval = new float[10];

		// Define o tamanho e posiçoes dos rectangulos
		Set_measures ();

		z_max = initialcoor.z + (rect_height / 2);
		z_min = initialcoor.z - (rect_height / 2);

		// Define um valor entre 0 e 1 tendo em conta a posiçao dos rectangulos
		Scale ();

		// Define a funçao de transferencia para os vectores
		//Transfer_func ();
		Adapt_1D ();

		StartCoroutine(UpdateFunction());
	}

	IEnumerator UpdateFunction()
	{
		while(true)
		{
			ManageBlocks();
			yield return new WaitForSeconds(2);
		}
	}

	void ManageBlocks () {
	
		float[] newpos = new float[10];
		bool changed = false;

		for (int i=0; i < newpos.Length; i++) {
			newpos[i] = rects[i].transform.position.z;

			if(worldz[i]!= newpos[i]){
				changed = true;
			}
		}

		if (changed) {
			worldz = newpos;
			Scale();
			Transfer_func();
		}

	}

	void Set_measures(){

		for (int i=0; i < rects.Length; i++) {

			Vector3 scale;
			scale.x = rect_lenght;
			scale.z = rect_height;
			scale.y = 0.001f;

			rects[i].transform.localScale = scale; 

			Vector3 worldpos = new Vector3 (initialcoor.x + (rect_lenght / 2)*(2*i + 1), 3f, initialcoor.z + (rect_height / 2));
			rects[i].transform.position = worldpos;

			worldz[i]= worldpos.z;
		}
	}

	void screenBounds () {
		
		Vector3 screenPosfi = touchcamera.WorldToScreenPoint(touchcamera.transform.position);
		
		centralpixel = screenPosfi;
		
		screen_height = touchcamera.pixelHeight;
		screen_lenght = touchcamera.pixelWidth;
		
		initialcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x - screen_lenght / 2, centralpixel.y - screen_height / 2, 0f));
		finalcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x + screen_lenght / 2, centralpixel.y + screen_height / 2, 0f));
		
		plane_height = finalcoor.z - initialcoor.z;
		plane_lenght = finalcoor.x - initialcoor.x;
	}

	void Scale(){

		for (int i=0; i< worldz.Length; i++) {

			float cval = worldz[i] - z_min;

			scaledval[i] = cval / rect_height;
			if(scaledval[i]<0)scaledval[i]=0;
		}
	}

	void Transfer_func(){

		float div = 255f / 10f;

		for (int i = 0; i< scaledval.Length; i++) {

			int one = Mathf.RoundToInt (i * div);
			int two = Mathf.RoundToInt ((i + 1) * div);

			for (int j= one; j<= two; j++) {
				if(scaledval[i] < 0f){
					trans_func [j] = 0;
				}else{
					trans_func [j] = scaledval [i];
				}
			}
		}

		if (intens) {
			tabs.SendMessage ("Send_function_opac_array", trans_func, SendMessageOptions.DontRequireReceiver);
		} else {
			tabs.SendMessage ("Send_function_grad_array", trans_func, SendMessageOptions.DontRequireReceiver);
		}
	}

	void Adapt_1D (){

		//declaracao do vector que importei do cursor
		float[] transf_vector;
		if (intens) {
			transf_vector = customs.funcao_transferencia;
		} else {
			transf_vector = customs.funçao_trans_grad;
		}

		float div = 255f / 10f;
		
		for (int i = 0; i< 10; i++) {
			
			int one = Mathf.RoundToInt(i*div);
			int two = Mathf.RoundToInt((i+1)*div);

			float total = 0;
			float nr = 0;
			for(int j= one; j<= two; j++){
				if(transf_vector[j]!=0){
					total = total + transf_vector[j];
					nr = nr + 1f;
				}
			}

			float mean = total / nr;
			if (mean <= 1f && mean >= 0f) {
			} else {
				mean=0f;
			}
			Vector3 pos = rects [i].transform.position;
			pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
			rects [i].transform.position = pos;
		}
	}
}
