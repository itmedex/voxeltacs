﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ToggleSelect : MonoBehaviour {

	public GameObject mainCamera;
	private bool active = true;
	private bool activeSpatial = true;
	private bool activeSpatial2 = true;

	// Objectos da funçao de intensidades
	public GameObject slidingboxes;
	public Function_draw draw;
	
	// Objectos da funçao de gradientes
	public GameObject slidingboxesGrad;
	public Function_draw drawGrad;
	public bool touch = false;

	public GameObject Widget;

	void Start () {
		if (!touch) {
			draw.unlocked = true;
			drawGrad.unlocked = true;
			slidingboxes.SetActive (false);
			slidingboxesGrad.SetActive (false);
		}
	}

	public void ToggleSlideInt(bool value)
	{
		//value is initially true, when first clicked
		draw.unlocked = !value;
		slidingboxes.SetActive (value);
		if(value)
			slidingboxes.SendMessage ("Adapt_1D", SendMessageOptions.DontRequireReceiver);
	}

	public void ToggleSlideGrad(bool value)
	{
		//value is initially true, when first clicked
		drawGrad.unlocked = !value;
		slidingboxesGrad.SetActive (value);
		if(value)
			slidingboxesGrad.SendMessage ("Adapt_1D", SendMessageOptions.DontRequireReceiver);
	}

	public void ToogleSketchCanvas()
	{
		mainCamera.GetComponent<TouchMovement>().enabled = active;
		drawGrad.unlocked = !active;
		draw.unlocked = !active;
		slidingboxes.SetActive (false);
		slidingboxesGrad.SetActive (false);
		active = !active;
	}

	public void ToogleSpatial()
	{
		GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
		nex.SendMessage ("SendSpatialSwitch", activeSpatial.ToString(), SendMessageOptions.DontRequireReceiver);
		activeSpatial = !activeSpatial;
	}

	public void ToogleSpatial2()
	{
		Widget.SetActive(activeSpatial2);
		GameObject nex = GameObject.FindGameObjectWithTag ("Nexus");
		nex.SendMessage ("SendSpatialSwitch2", activeSpatial2.ToString(), SendMessageOptions.DontRequireReceiver);
		activeSpatial2 = !activeSpatial2;
	}
}
