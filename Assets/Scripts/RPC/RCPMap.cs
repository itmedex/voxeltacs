﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RCPMap : MonoBehaviour {

	public string address = "127.0.0.1"; // localhost
	public string port = "99999";
	
	public bool server = false;

	private float next_time;

	public Text server_text;
	public Text client_text;
	public Text input_text;

	void Awake(){
		Application.runInBackground = true;
		DontDestroyOnLoad(transform.gameObject);
	}

	void Start() {
		if(server)
			Set_server ();
	}
	
	void Update(){

		if (Network.peerType != NetworkPeerType.Disconnected) {
			if (Network.peerType == NetworkPeerType.Client)
				if(client_text!=null)
					client_text.text = "Client Connected";
			
			if (Network.peerType == NetworkPeerType.Server)
				if(server_text!=null)
					server_text.text = "Server Running";
		}
	}

	void SendSpatialSwitch(string value){
		GetComponent<NetworkView> ().RPC ("RelaySpatialSwitch", RPCMode.All, value);
	}

	[RPC]
	void RelaySpatialSwitch(string value){	
		if (Network.peerType == NetworkPeerType.Server) {
			GameObject[] custom = GameObject.FindGameObjectsWithTag ("Spatial");
			custom [0].GetComponent<OptiTrackTracking> ().enabled = ((value.Equals ("True")) ? true : false);
			custom [1].GetComponent<SpatialMovement> ().enabled = ((value.Equals ("True")) ? true : false);
		}
	}

	void SendSpatialSwitch2(string value){
		GetComponent<NetworkView> ().RPC ("RelaySpatialSwitch2", RPCMode.All, value);
	}
	
	[RPC]
	void RelaySpatialSwitch2(string value){	
		if (Network.peerType == NetworkPeerType.Server) {
			GameObject[] custom = GameObject.FindGameObjectsWithTag ("Spatial");
			custom [1].GetComponent<SpatialMovement> ().enabled = ((value.Equals ("True")) ? false : true);
			custom [1].GetComponent<SpatialSlicing> ().enabled = ((value.Equals ("True")) ? true : false);
		}
	}

	void SendSlicesData(Texture2D image){
		byte[] bytesToSend = image.EncodeToJPG();
		if (bytesToSend != null && bytesToSend.Length > 0)
			GetComponent<NetworkView> ().RPC ("RelaySlicesData", RPCMode.All, bytesToSend);
		else
			Debug.LogError("Bad length of bytes to send.");
	}
		
	public List<Texture2D> receivedTextures = new List<Texture2D>();
	public List<Texture2D> getSlices() {
		return receivedTextures;
	}
	[RPC]
	void RelaySlicesData(byte[] imageBytes){
		if (imageBytes.Length < 1)
		{
			Debug.LogError("Received bad byte count from network.");
			return;
		}

		Texture2D recTexture = new Texture2D(1, 1/*, TextureFormat.RGB24, true*/);
		recTexture.LoadImage(imageBytes);
		receivedTextures.Add(recTexture);
		//GameObject custom = GameObject.FindGameObjectWithTag("customfunction");
		//custom.SendMessage("DefineMatrixInt",array, SendMessageOptions.DontRequireReceiver);
	}
	
	// Array de opacidades
	void Send_function_opac_array(float[] transfer){
		
		//if (Network.peerType == NetworkPeerType.Server) {
			string message = "";
			
			for (int i=0; i<256; i++) {
					float opac = transfer [i];
				//int opac_int = Mathf.RoundToInt(opac);
					message += opac.ToString () + "-";
			}
			//print ("opac matrix send");
			GetComponent<NetworkView> ().RPC ("Relay_function_opac_array", RPCMode.All, message);
		//}
	}

	[RPC]
	void Relay_function_opac_array(string message){
		
		//if (Network.peerType == NetworkPeerType.Client) {

			//print (message);

			string[] components = new string[1];
			float[] array = new float[256];
			
			components = message.Split ("-" [0]);

			for (int i=0; i<256; i++) {
				//print (components[i]);
				array [i] = float.Parse(components [i]);
			}
			
			// mandar mensagem com a array
			//print ("recebeu o array de opacidades");
			GameObject custom = GameObject.FindGameObjectWithTag("customfunction");
			custom.SendMessage("DefineMatrixInt",array, SendMessageOptions.DontRequireReceiver);
		//}
	}

	// Array de gradientes
	void Send_function_grad_array(float[] transfer){
		
		//if (Network.peerType == NetworkPeerType.Server) {
			string message = "";
			
			for (int i=0; i<256; i++) {
				float opac = transfer [i];
				//int opac_int = Mathf.RoundToInt(opac);
				message += opac.ToString () + "-";
			}
			//print ("grad send");
			GetComponent<NetworkView> ().RPC ("Relay_function_grad_array", RPCMode.All, message);
		//}
	}

	[RPC]
	void Relay_function_grad_array(string message){
		
		//if (Network.peerType == NetworkPeerType.Client) {
			string[] components = new string[1];
			float[] array = new float[256];
			
			components = message.Split ("-" [0]);
			
			for (int i=0; i<256; i++) {
				array [i] = float.Parse(components [i]);
			}
			
			//print ("recebeu o array de gradientes");
			GameObject custom = GameObject.FindGameObjectWithTag("customfunction");
			custom.SendMessage("DefineMatrixGrad",array, SendMessageOptions.DontRequireReceiver);
		//}
	}

	// Vector de Cores
	void Send_function_color_array(float[] transfer){
		
		if (Network.peerType == NetworkPeerType.Client) {
			string message = "";
			
			for (int i=0; i<transfer.Length; i++)
				message += transfer[i].ToString() + ",";

			print ("RCPMap::SendColorArray::Client: " + message);
			GetComponent<NetworkView> ().RPC ("Relay_function_color_array", RPCMode.All, message);
		}
	}
	
	[RPC]
	void Relay_function_color_array(string message){

		if (Network.peerType == NetworkPeerType.Server) {
			string[] components = new string[1];
			components = message.Split ("," [0]);
			float[] color_func = new float[components.Length];
			
			for (int i=0; i<components.Length-1; i++)
				color_func[i] = float.Parse(components[i]);

			GameObject custom = GameObject.FindGameObjectWithTag("color_picker_camera");
			print("RCPMap::SendColorUpdate::Server");
			custom.SendMessage("Receive_color_update", color_func ,SendMessageOptions.DontRequireReceiver);
		}
	}

	// Transformadas
	void Send_function_slicing(float[] data){
		
		//if (Network.peerType == NetworkPeerType.Server) {
		string message = "";
		
		message += data[0].ToString () + ",";
		message += data[1].ToString ();
		
		GetComponent<NetworkView> ().RPC ("Relay_function_slicing", RPCMode.All, message);
		//}
	}

	[RPC]
	void Relay_function_slicing(string message){
		
		//if (Network.peerType == NetworkPeerType.Client) {
		
		string[] components = new string[1];
		float[] data = new float[2];
		
		components = message.Split ("," [0]);
		
		data[0] = float.Parse (components [0]);
		data[1] = float.Parse (components [1]);
		
		GameObject custom = GameObject.FindGameObjectWithTag("MainCamera");
		custom.SendMessage("setSlicing", data ,SendMessageOptions.DontRequireReceiver);
		//}
	}

	void SendWidgetFace(int face){
		
		//if (Network.peerType == NetworkPeerType.Server) {
		GetComponent<NetworkView> ().RPC ("RelayWidgetFace", RPCMode.All, face);
		//}
	}
	
	[RPC]
	void RelayWidgetFace(int face){
		
		GameObject custom = GameObject.FindGameObjectWithTag("MainCamera");
		custom.SendMessage("setWidgetFace", face ,SendMessageOptions.DontRequireReceiver);
	}

	// Transformadas
	void Send_function_tranform(Transform transfer){
		
		//if (Network.peerType == NetworkPeerType.Server) {
			string message = "";
			
			message += transfer.position.x.ToString () + ",";
			message += transfer.position.y.ToString () + ",";
			message += transfer.position.z.ToString () + ",";
			
			message += transfer.rotation.x.ToString () + ",";
			message += transfer.rotation.y.ToString () + ",";
			message += transfer.rotation.z.ToString () + ",";
			message += transfer.rotation.w.ToString ();
			
			GetComponent<NetworkView> ().RPC ("Relay_function_transform", RPCMode.All, message);
		//}
	}
	
	[RPC]
	void Relay_function_transform(string message){
		
		//if (Network.peerType == NetworkPeerType.Client) {
			
			string[] components = new string[1];
			Vector3 position = new Vector3 (0, 0, 0);
			Quaternion rotation = new Quaternion (0, 0, 0, 0);
			
			components = message.Split ("," [0]);

			position.x = float.Parse (components [0]);
			position.y = float.Parse (components [1]);
			position.z = float.Parse (components [2]);

			rotation.x = float.Parse (components [3]);
			rotation.y = float.Parse (components [4]);
			rotation.z = float.Parse (components [5]);
			rotation.w = float.Parse (components [6]);
			
			Transform transf = transform;
			transf.position = position;
			transf.rotation = rotation;

			GameObject custom = GameObject.FindGameObjectWithTag("MainCamera");
			custom.SendMessage("Set_trans", transf ,SendMessageOptions.DontRequireReceiver);

		//}
	}

	// Zoom
	void Send_function_fov(float fov){
		
		//if (Network.peerType == NetworkPeerType.Server) 
		//{
			GetComponent<NetworkView> ().RPC ("Relay_function_fov", RPCMode.All, fov);
		//}
	}
	
	[RPC]
	void Relay_function_fov(float fov){
		
		//if (Network.peerType == NetworkPeerType.Client)
		//{
			//print ("o fov e " + fov);
			GameObject custom = GameObject.FindGameObjectWithTag("MainCamera");
			custom.SendMessage("Set_fov", fov ,SendMessageOptions.DontRequireReceiver);
		//}
	}


	// Matrix de cores
	void Send_function_color_matrix(float[] transfer){
		
		if (Network.peerType == NetworkPeerType.Client) {
			string message = "";
			
			for (int i=0; i<transfer.Length; i++) {
				message += transfer[i].ToString() + ",";
			}

			GetComponent<NetworkView> ().RPC ("Relay_function_color_matrix", RPCMode.All, message);
		}
	}
	
	[RPC]
	void Relay_function_color_matrix(string message){
		
		if (Network.peerType == NetworkPeerType.Server) {
			string[] components = new string[1];
			components = message.Split ("," [0]);
			float[] color_func = new float[components.Length];
			
			for (int i=0; i<components.Length-1; i++) {
				color_func[i] = float.Parse(components[i]);
			}

			GameObject custom = GameObject.FindGameObjectWithTag("color_picker_camera");
			custom.SendMessage("Receive_color_update", color_func ,SendMessageOptions.DontRequireReceiver);
		}
	}
	
	// Converter os arrays em strings e depois converter as strings novamente em arrays.
	// Sao precisas 4 funçoes. Uma matrix de opacidades, um vector de cores, uma matrix de cores e uma transformada.
	void Send_opac_map(Vector2[] transfer){
		
		if (Network.peerType == NetworkPeerType.Server) {
			string message = "";

			foreach(Vector2 vec in transfer){
				message += vec.x.ToString() + "," + vec.y.ToString() + ",";
			}
			
			GetComponent<NetworkView> ().RPC ("Relay_opac_map", RPCMode.All, message);
		}
	}

	[RPC]
	void Relay_opac_map(string message){
		
		if (Network.peerType == NetworkPeerType.Client) {
			
			string[] components = new string[1];
			components = message.Split ("," [0]);
			Vector2[] vector = new Vector2[(components.Length-1)/2];

			for(int i=0; i<vector.Length; i++){

				vector[i].x = float.Parse( components[(i*2)]);
				vector[i].y = float.Parse( components[(i*2) + 1]);
				//print(vector[i].x + " " + vector[i].y);
			}

			GameObject custom = GameObject.FindGameObjectWithTag("2dcanvas");
			custom.SendMessage("Resend_lines", vector ,SendMessageOptions.DontRequireReceiver);
		}
	}

	public void Set_server(){
		if (Network.peerType == NetworkPeerType.Disconnected) {
			Network.InitializeServer (32, int.Parse (port), true);
		}
	}
	
	public void Set_client(){
		if (Network.peerType == NetworkPeerType.Disconnected) {
			if (Time.time > next_time) {
				print ("connecting");
				next_time = Time.time + 2f;
				address = input_text.text;
				Network.Connect (address, int.Parse (port));
			}
		}
	}
}
