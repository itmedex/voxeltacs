using UnityEngine;
using System.Collections;

public class initFileBrowser : MonoBehaviour {

	string[] files;
	private VolumeData dataScript;

	//skins and textures
	public GUISkin skins;
	public Texture2D file,folder,back,drive,search;

	//initialize file browser
	FileBrowser fb = new FileBrowser();
	string output = "no file";
	// Use this for initialization
	void Start () {

		//setup file browser style
		fb.guiSkin = skins; //set the starting skin
		//set the various textures
		fb.fileTexture = file; 
		fb.directoryTexture = folder;
		fb.backTexture = back;
		fb.driveTexture = drive;
		fb.searchTexture = search;
		//show the search bar
		fb.showSearch = true;
		//search recursively (setting recursive search may cause a long delay)
		fb.searchRecursively = true;
		fb.setLayout (0);
		dataScript = GameObject.Find("VolumeData").GetComponent<VolumeData>();
	}
	
	void OnGUI(){
		//draw and display output
		if(fb.draw()){ //true is returned when a file has been selected
			//the output file is a member if the FileInfo class, if cancel was selected the value is null
			output = (fb.outputFile==null)?null:fb.outputFile.ToString();
			//Debug.Log (output);
			//Debug.Log (fb.outputFile.Directory.ToString());
			if(output != null) {
				files = System.IO.Directory.GetFiles(fb.outputFile.Directory.ToString(), "*.jpg");
				dataScript.setPath(fb.outputFile.Directory.ToString());
				dataScript.setSlices(files);
			}
		}
	}
	
}
