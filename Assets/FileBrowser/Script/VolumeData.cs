﻿using UnityEngine;
using System.Collections;

public class VolumeData : MonoBehaviour {

	private Texture2D[] _slices;
	private string loadPath;
	private string pathPreFix = @"file://";

	void Awake() {
		DontDestroyOnLoad (this);
	}

	public Texture2D[] getSlices() {
		return _slices;
	}

	public void setSlices(string[] files) {
		LoadImages (files);
	}

	public void setPath(string loadPath) {
		this.loadPath = loadPath;
	}

	private void LoadImages(string[] files){
		_slices = new Texture2D[files.Length];
		int dummy = 0;
		foreach (string tstring in files) {
			string pathTemp = pathPreFix + tstring;
			print ("####=> " + pathTemp);
			WWW www = new WWW (pathTemp);
			//yield return www;
			Texture2D texTmp = new Texture2D (0,0);  
			www.LoadImageIntoTexture (texTmp);
			_slices [dummy] = texTmp;
			dummy++;
		}
	}
	
}
