﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Net;

public class NetworkTransition : MonoBehaviour {
	
	[SerializeField]
	private float m_minDuration = 1.0f;

	public Text ipAddress;

	public void Start() {
		if(ipAddress != null)
			ipAddress.text = Network.player.ipAddress;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Escape) == true)
		{
			Exitapp();
		}
	}
	
	public void Exitapp() {
		Application.Quit();
	}

	public void LoadMenuTabs() {
		Application.LoadLevel ("MenuVoxelTabs");
	}

	public void LoadMenuTips() {
		Application.LoadLevel("MenuVoxelTips");
	}

	public void LoadTips() {
		Application.LoadLevel("InitiateTips");
	}

	public void LoadTabs() {
		Application.LoadLevel("InitiateTabs");
	}

	public void LoadSettings() {
		Application.LoadLevel("SettingsTips");
	}

	public void LoadSketch() {
		StartCoroutine(LoadSceneAsync("SketchCanvas", "Tabs"));;
	}

	public void LoadTouch() {
		StartCoroutine(LoadSceneAsync("TouchCanvas", "Tabs"));
	}

	public void LoadSlices() {
		StartCoroutine(LoadSceneAsync("SliceCanvas", "Tabs"));
	}

	public void LoadVolume() {
		StartCoroutine(LoadSceneAsync("VolumeTips", "Tips"));
	}

	public void LoadPelvis() {
		StartCoroutine(LoadSceneAsync("VolumePelvis", "Tips"));
	}

	public void LoadHead() {
		StartCoroutine(LoadSceneAsync("VolumeHead", "Tips"));
	}


	public void Load2DImages() {
		StartCoroutine(LoadSceneAsync("ImagingCanvas", "Tabs"));
	}

	public void LoadSpatialSlicing() {
		StartCoroutine(LoadSceneAsync("SpatialCanvas", "Tabs"));
	}


	public void LoadBrowser() {
		Application.LoadLevel("FileBrowser");
	}

	// ********************************************************************
	// Function:	LoadScene()
	// Purpose:		Loads the supplied scene
	// ********************************************************************
	public IEnumerator LoadSceneAsync(string sceneName, string app)
	{
		// Load loading screen
		yield return Application.LoadLevelAsync("LoadingScreen" + app);
		
		// !!! unload old screen (automatic)
		
		float endTime = Time.time + m_minDuration;
		
		// Load level async
		yield return Application.LoadLevelAdditiveAsync(sceneName);
		
		while (Time.time < endTime)
			yield return null;
		
		// !!! unload loading screen
		LoadingSceneManager.UnloadLoadingScene();
	}
}
