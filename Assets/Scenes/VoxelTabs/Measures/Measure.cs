﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Measure : MonoBehaviour {

	private LineRenderer line;
//	private List<Vector3> pointsList;
	private Vector3 mousePos;
	private bool unlocked;
	private bool id_set;
	private int main_finger_id;
	//public GameObject marker;

	void Awake()
	{
		// Create line renderer component and set its property
		line = gameObject.AddComponent<LineRenderer>();
		line.material =  new Material(Shader.Find("Particles/Additive"));
		line.material.color = Color.green;
		line.SetWidth(0.1f,0.1f);
		line.SetVertexCount(2);
		line.SetColors(Color.green, Color.green);
		line.useWorldSpace = true;
//		pointsList = new List<Vector3>();
		unlocked = true;
	}

	void Update () {
		
		if (unlocked) {
			
			if (TuioInput.touchCount == 1) {
				if(!id_set){
					main_finger_id = TuioInput.touches[0].fingerId;
					id_set = true;
				}
				
				// Mudar para usar o finger id
				//Touch touch = TuioInput.touches [0];
				Touch touch = new Touch();
				bool set = false;
				foreach( Touch touch_1 in TuioInput.touches){
					if(touch_1.fingerId == main_finger_id) {
						touch = touch_1;
						set=true;
					}
				}
				// se nao encontrar nenhum touch com esse id, entao e porque levantou esse dedo
				// nesse caso, o primeiro dedo no vector vai ser usado
				if(!set){
					main_finger_id = TuioInput.touches[0].fingerId;
					touch = TuioInput.touches[0];
				}

				if(touch.phase == TouchPhase.Began)
				{
					mousePos = Camera.main.ScreenToWorldPoint(TuioInput.touches[0].position);
					mousePos.z=0;
					line.SetPosition (0,mousePos);
					line.SetPosition (1,mousePos);
					//GameObject child = Instantiate (marker, mousePos,Quaternion.identity) as GameObject;
					//child.transform.SetParent(gameObject.transform, true);
				}
				else if(touch.phase == TouchPhase.Ended) {
					mousePos = Camera.main.ScreenToWorldPoint(TuioInput.touches[0].position);
					mousePos.z=0;
					//GameObject child = Instantiate (marker, mousePos,Quaternion.identity) as GameObject;
					//child.transform.SetParent(gameObject.transform, true);
					unlocked = false;
				}
				// Drawing line when mouse is moving(presses)
				if(touch.phase == TouchPhase.Moved)
				{
					mousePos = Camera.main.ScreenToWorldPoint(TuioInput.touches[0].position);
					mousePos.z=0;
					line.SetPosition (1,mousePos);
				}
			}
		}
	}
}