﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InteractionMovement : MonoBehaviour {

	public GameObject measureObject;
	public GameObject angleObject;

	public void takeMeasure() {
		GameObject child = Instantiate (measureObject) as GameObject;
		child.transform.parent = gameObject.transform;
	}

	public void takeAngle() {
		GameObject child = Instantiate (angleObject) as GameObject;
		child.transform.parent = gameObject.transform;
	}
}