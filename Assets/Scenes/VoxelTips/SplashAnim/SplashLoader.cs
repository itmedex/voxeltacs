﻿using UnityEngine;
using System.Collections;

public class SplashLoader : MonoBehaviour {

	public float time = 3.0f;

	// Use this for initialization
	void Start () {
		StartCoroutine ("Countdown");
	}

	private IEnumerator Countdown() {
		yield return new WaitForSeconds(time);
		Application.LoadLevel (1);
	}
}
