﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SlicesRPC : MonoBehaviour {

	private RayMarching volume;
	private int posSlices;
	public bool tabs;
	public SpriteRenderer slice;

	private float screen_height;

	// Tablet Connection Object
	GameObject tab;

	// Use this for initialization
	void Start () {
		volume = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<RayMarching>();
		tab = GameObject.FindGameObjectWithTag ("Nexus");
		if (tabs) {
			//screenBounds ();
			//slice.GetComponent<RectTransform> ().sizeDelta = new Vector2 (screen_height, screen_height);
			updateSlicesData();
			Destroy (GameObject.FindGameObjectWithTag ("Finish"));
		}
	}

	// Determinar dimensoes do ecra
	void screenBounds () {
		Camera cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
		screen_height = cam.pixelHeight;
	}

	public void sendSlicesData() {
		foreach (Texture2D text in volume.getSlices ())
			tab.SendMessage ("SendSlicesData", text, SendMessageOptions.DontRequireReceiver);
	}

	public void updateSlicesData() {
		if (tab.GetComponent<RCPMap> ().getSlices().Count != 0) {
			posSlices = (int)((tab.GetComponent<RCPMap> ().getSlices().Count)/2);
			Texture2D text = tab.GetComponent<RCPMap> ().getSlices()[posSlices];
			slice.sprite = Sprite.Create(text, new Rect(0, 0, text.width, text.height), new Vector2(0.5f, 0.5f));
			//slice.texture = tab.GetComponent<RCPMap> ().getSlices()[posSlices];
		}
	}

	public void forwardVolume() {
		if (tab.GetComponent<RCPMap>().getSlices()[posSlices+1] != null) {
			Texture2D text = tab.GetComponent<RCPMap> ().getSlices()[++posSlices];
			slice.sprite = Sprite.Create(text, new Rect(0, 0, text.width, text.height), new Vector2(0.5f, 0.5f));
		}
	}

	public void backwardVolume() {
		if (posSlices != 0 && tab.GetComponent<RCPMap>().getSlices()[posSlices-1] != null) {
			Texture2D text = tab.GetComponent<RCPMap> ().getSlices()[--posSlices];
		  	slice.sprite = Sprite.Create(text, new Rect(0, 0, text.width, text.height), new Vector2(0.5f, 0.5f));
		}
	}
}
