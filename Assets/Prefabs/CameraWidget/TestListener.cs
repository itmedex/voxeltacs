﻿using UnityEngine;
using System.Collections;

public class TestListener : MonoBehaviour
{
    public CameraWidget widget;

    // Use this for initialization
    void Start()
    {
        widget.OnCameraChanged += widget_OnCameraChanged;
    }

    void widget_OnCameraChanged(Camera camera, string view, Vector3 direction)
    {
        Debug.Log("Registered " + view + "(" + direction + ") on " + camera.name);
       //rigidbody.AddForce(direction * 100f);
    }
}
