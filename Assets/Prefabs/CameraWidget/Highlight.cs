﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Highlights the object with a particular material,
/// and memorizes the original material to reset it.
/// </summary>
public class Highlight : MonoBehaviour
{
    // public Color highlightColor;
    // Saved so we can reset when de-highlight occurs
    private Color startColor;

    private Color washedColor;

    // If true, object is highlighted
    private bool highlighted;
    // private Color currentColor;

    void Start()
    {
        startColor = GetComponent<Renderer>().material.color;
        washedColor = new Color(
            (startColor.grayscale + startColor.r * 0.3f),
            (startColor.grayscale + startColor.g * 0.3f),
            (startColor.grayscale + startColor.b * 0.3f)) * 0.4f;
        washedColor.a = 0.8f;

        GetComponent<Renderer>().material.color = washedColor;
        highlighted = false;
    }

    public void SetHighlight(bool highlight)
    {
        this.highlighted = highlight;

        GetComponent<Renderer>().material.color = highlighted ? startColor : washedColor;

		StartCoroutine (ResetColor());
		//ResetColor ();

        //if (!highlight)
          // renderer.material.color = startColor;
    }

	IEnumerator ResetColor()
	{
		yield return new WaitForSeconds(1);
		this.highlighted = false;
		GetComponent<Renderer>().material.color = highlighted ? startColor : washedColor;

	}
}
